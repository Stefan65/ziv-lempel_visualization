# Visualization of compression algorithms (Ziv-Lempel)

This repository contains some implementations and visualizations of the compression algorithms by Jacob Ziv and Abraham Lempel - namely LZ77 and LZ78 - and LZW, the version modified by Terry A. Welch.

The algorithms are implemented using pure Python and can be called using a completely JSON-based POST API (based upon Flask) or directly using the Python modules from within your code. All the steps will be exposed. The frontend is using HTML and CSS with JavaScript for the interaction. In the frontend all steps can be accessed in the usual order, but backwards as well. The frontend is only one possible way for a nice representation and should be considered as a demonstration of the possibilities. In fact it should be rather simple to use the underlying Python API in conjunction with own templates to create a `beamer` presentation with LaTeX.

The results will always be tuples, which would have to be encoded to get a actual result for writing it to a file. For this reason you will not really be able to see the compression ratio for the implemented algorithms, as this depends on the encoding techniques used.

It is not recommended to use these modules for actual compression as all steps including a short reasoning for the current decision, the current dictionary content etc. will be saved and returned. Instead the target audience probably are students or people who want to get a better understanding of the algorithms, although some basic knowledge is recommended to avoid confusion. The code itself has been developed as part of an university project.

The implementation is based on the explanations in:

> Khalid Sayood. *Introduction to Data Compression.* 5th edition. The Morgan Kaufmann Series in Multimedia Information and Systems. Cambridge, MA: Elsevier, 23th October 2017. ISBN: 978-0-12-809474-7. URL: https://www.elsevier.com/books/introduction-to-data-compression/sayood/978-0-12-809474-7.

A short note regarding the last tuple element: The last tuple element will always be the last character of the input text. This means that some matches may be reduced by one when the character from the last tuple would be empty otherwise. An alternative implementation could use `\0` to terminate the strings (which avoids shortening the last match), but this has not been implemented for now.

## Running

Make sure you have

* Python 3
* [Flask](https://flask.palletsprojects.com/)
* [Flask-JSON](https://github.com/skozlovf/flask-json)
* [rply](https://github.com/alex/rply) (currently unused)

installed on your device.

Afterwards you can run `python3 -m ziv_lempel` from the terminal to start the server. The data will be provided at http://localhost:5000/.

## Usage in production

At the moment, all files inside the `ziv_lempel/static` directory are served by Flask automatically. While this is no problem for development purposes, these should be served by the real web server in production.

Additionally, the integrated WSGI server should be replaced by an external one based on a reverse proxy and [Gunicorn](https://gunicorn.org/) for example in production. Please make sure to disable the debugging mode inside the `__main__.py` file in this case as well - otherwise stacktrace will be returned for the JSON API and all requests would be logged.

## Development Tasks

### Tests

Just run `python3 -m unittest discover --verbose --start-directory tests` from the root directory of this repository.

### Code style

After installing [black](https://github.com/psf/black), you should be able to run `black .` from the root directory of this repository to apply auto-formatting.

Comments will not be reformatted, so please make sure to use a maximum line length of 88 characters (as done by the `black` formatter for the remaining code) manually.

To check the formatting itself, use `flake8` after installing [it](https://gitlab.com/pycqa/flake8).

### Documentation

You need the following Python modules:

* [Sphinx](http://www.sphinx-doc.org/en/stable/)
* [sphinxcontrib.httpdomain](https://github.com/sphinx-contrib/httpdomain/)
* [sphinxcontrib.openapi](https://github.com/sphinx-contrib/openapi)
* [sphinx_rtd_theme](https://github.com/rtfd/sphinx_rtd_theme/)
* [GitPython](https://github.com/gitpython-developers/GitPython)

Then you should be able to run `make clean && make html` from the `docs` directory to get the documentation in HTML format. Use `make latex` to create LaTeX source files.

## Citing

If you want to cite this code in your own paper, feel free to write me a message. I will provide you with the corresponding entry in this case.

## License

The provided code is distributed under the terms of the [3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause).
