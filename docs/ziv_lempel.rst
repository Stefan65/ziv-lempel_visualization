ziv_lempel package
==================

This page contains the inline documentation, generated from the code using Sphinx.

Python-based API
----------------

The Python-based API uses real method parameters for specifying the input data and avoids additional output wrapping. Errors will be raised as regular Python errors.

`ziv_lempel.algorithms.lz77` -- LZ77
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.algorithms.lz77

`ziv_lempel.algorithms.lz78` -- LZ78
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.algorithms.lz78

`ziv_lempel.algorithms.lzw` -- LZW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.algorithms.lzw

`ziv_lempel.algorithms.tuples` -- Tuple parser
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.algorithms.tuples

`ziv_lempel.algorithms.utils` -- Utilities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.algorithms.utils

Web-based API
-------------

The web-based API uses dictionaries for specifying the input data and wraps the output. Errors will be available from the output dictionary.

`ziv_lempel.api.configuration` -- Configuration values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.api.configuration

`ziv_lempel.api.lz77_api` -- LZ77 API
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.api.lz77_api

`ziv_lempel.api.lz78_api` -- LZ78 API
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.api.lz78_api

`ziv_lempel.api.lzw_api` -- LZW API
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ziv_lempel.api.lzw_api
