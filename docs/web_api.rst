Web-based API endpoints
=======================

This page documents the API endpoints provided by the Flask application.

Overview
--------

Each endpoint can be accessed with using either `GET` or `POST` requests.

For `GET` requests, just pass the parameters as usual:

.. sourcecode:: http

   GET /endpoint?number=1&text=test HTTP/1.1
   Host: localhost:5000

For `POST` requests, you have to pass the parameters as a JSON dictionary and set the corresponding content type:

.. sourcecode:: http

   POST /endpoint HTTP/1.1
   Host: localhost:5000
   Content-Type: application/json; charset=utf-8

   {"number": 1, "text": "test"}

The following endpoints are available at the moment:

.. qrefflask:: ziv_lempel.__main__:APP
   :undoc-endpoints: landing_page
   :undoc-static:

Details
-------

Let us get into the details for each of the endpoints. For simplicity, only the `GET` versions will be described, while the parameters are the same for the `POST` versions.

Each of the endpoints is available in the path `server/api/`, where `server` is `http://localhost:5000` for example. A complete URL would be

.. sourcecode:: none

   http://localhost:5000/api/lz77/compress
   
which corresponds to the LZ77 compression endpoint of the local server.

Unfortunately Sphinx does not fully support OpenAPI documentation, especially with the return values. For this reason, you may want to have a look at the rendered version of the specification on GitLab.

LZ77 Compression
~~~~~~~~~~~~~~~~

.. raw:: html

    <div id="post--api-lz77-compress"></div>
    <div id="get--api-lz77-compress"></div>

.. openapi:: specifications/openapi.yaml
   :paths:
        /lz77/compress

LZ77 Decompression
~~~~~~~~~~~~~~~~~~

.. raw:: html

    <div id="post--api-lz77-decompress"></div>
    <div id="get--api-lz77-decompress"></div>

.. openapi:: specifications/openapi.yaml
   :paths:
        /lz77/decompress

LZ78 Compression
~~~~~~~~~~~~~~~~

.. raw:: html

    <div id="post--api-lz78-compress"></div>
    <div id="get--api-lz78-compress"></div>

.. openapi:: specifications/openapi.yaml
   :paths:
        /lz78/compress

LZ78 Decompression
~~~~~~~~~~~~~~~~~~

.. raw:: html

    <div id="post--api-lz78-decompress"></div>
    <div id="get--api-lz78-decompress"></div>

.. openapi:: specifications/openapi.yaml
   :paths:
        /lz78/decompress

LZW Compression
~~~~~~~~~~~~~~~

.. raw:: html

    <div id="post--api-lzw-compress"></div>
    <div id="get--api-lzw-compress"></div>

.. openapi:: specifications/openapi.yaml
   :paths:
        /lzw/compress

LZW Decompression
~~~~~~~~~~~~~~~~~

.. raw:: html

    <div id="post--api-lzw-decompress"></div>
    <div id="get--api-lzw-decompress"></div>

.. openapi:: specifications/openapi.yaml
   :paths:
        /lzw/decompress
