Visualization of compression algorithms (Ziv-Lempel)
====================================================

This repository contains some implementations and visualizations of the compression algorithms by Jacob Ziv and Abraham Lempel - namely LZ77 and LZ78 - and LZW, the version modified by Terry A. Welch.

The algorithms are implemented using pure Python and can be called using a completely JSON-based POST API (based upon Flask) or directly using the Python modules from within your code. All the steps will be exposed. The frontend is using HTML and CSS with JavaScript for the interaction. In the frontend all steps can be accessed in the usual order, but backwards as well.

The results will always be tuples, which would have to be encoded to get a actual result for writing it to a file. For this reason you will not really be able to see the compression ratio for the implemented algorithms, as this depends on the encoding techniques used.

It is not recommended to use these modules for actual compression as all steps including a short reasoning for the current decision, the current dictionary content etc. will be saved and returned. Instead the target audience probably are students or people who want to get a better understanding of the algorithms, although some basic knowledge is recommended to avoid confusion. The code itself has been developed as part of an university project.

The implementation is based on the explanations in:

    Khalid Sayood. *Introduction to Data Compression.* 5th edition. The Morgan Kaufmann Series in Multimedia Information and Systems. Cambridge, MA: Elsevier, 23th October 2017. ISBN: 978-0-12-809474-7. URL: https://www.elsevier.com/books/introduction-to-data-compression/sayood/978-0-12-809474-7.

A short note regarding the last tuple element: The last tuple element will always be the last character of the input text. This means that some matches may be reduced by one when the character from the last tuple would be empty otherwise. An alternative implementation could use ``\0`` to terminate the strings (which avoids shortening the last match), but this has not been implemented for now.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ziv_lempel
   web_api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
