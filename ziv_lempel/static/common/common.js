/*
 * Common code shared between all algorithms.
 */

// Get the modal elements used for displaying error messages.
const modal = $("#modal");
const modalTitle = $("#modalTitle");
const modalBody = $("#modalBody");

// Get the output text elements as they are referenced quite often and do not change
// their IDs.
const stepNumberValue = $("#stepNumberValue");
const explanationValue = $("#explanationValue");
const outputArea = $("#outputArea");

// Some global variables which hold the most often used fields of the JSON response.
var jsonResponse = null;
var stepCount = -1;
var steps = null;

// Some global variables indicating the current stage of the visualization.
var stepNumber = -1;
var stepNumberSub = -1;
var isForward = true;

// Add the length indicator for the input text field.
$("#inputTextInput").maxlength({
    alwaysShow: true,
    limitReachedClass: "limitReached",
    separator: " out of ",
    preText: "You wrote ",
    postText: " characters.",
    validate: true,
});

// Add the length indicator for the compressed data input field.
$("#compressedDataInput").maxlength({
    alwaysShow: true,
    limitReachedClass: "limitReached",
    separator: " out of ",
    preText: "You wrote ",
    postText: " characters.",
    validate: true,
});

/*
 * Handle API errors.
 *
 * @param message The API message.
 */
function handleApiError(message) {
    modalTitle.text("API Error");
    modalBody.text(message);
    modal.modal("show");
}

/*
 * Handle AJAX errors.
 *
 * @param response The error message.
 */
function handleAjaxError(response) {
    modalTitle.text("Request Error");
    modalBody.text("The request could not be handled.");
    modal.modal("show");
}

/*
 * Change the step numbers depending on the current step number, sub-number and the
 * current direction.
 */
function changeStep() {
    // Avoid too large values.
    if (stepNumber == stepCount && stepNumberSub == 2 && isForward) {
        return;
    }

    // Avoid to small values.
    if (stepNumber == 1 && stepNumberSub == 1 && !isForward) {
        return;
    }

    // Backward movements need the opposite treatment when compared to regular
    // movements (forward).
    if (isForward) {
        if (stepNumberSub == 1) {
            stepNumberSub = 2;
        } else {
            stepNumberSub = 1;
            stepNumber += 1;
        }
    } else if (!isForward) {
        if (stepNumberSub == 1) {
            stepNumberSub = 2;
            stepNumber -= 1;
        } else {
            stepNumberSub = 1;
        }
    }
}
