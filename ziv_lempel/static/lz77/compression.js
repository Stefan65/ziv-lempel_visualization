/*
 * Simulation of data compression with LZ77 using a JSON API, client-side.
 *
 * This file contains the client-side code for communicating with the JSON API for
 * simulating/visualizing the LZ77 algorithm as per [Say17] using a stepwise approach.
 */

///////////////////////////////////////////////////////////
// CONFIGURATION                                         //
///////////////////////////////////////////////////////////

// Define the default values for the input section.
DEFAULT_SEARCH_BUFFER_SIZE = 10;
DEFAULT_LOOK_AHEAD_BUFFER_SIZE = 10;
DEFAULT_INPUT_TEXT = "Lorem ipsum";
DEFAULT_INITIAL_SEARCH_BUFFER = "NULL;";

// Some global variables which hold the most often used fields of the JSON response.
var searchBufferSize = -1;
var lookAheadBufferSize = -1;

// The current output triples. We are storing this as an array to allow removing
// elements without any hassle for the backwards version.
var currentOutput = [];

///////////////////////////////////////////////////////////
// SETUP                                                 //
///////////////////////////////////////////////////////////

// Perform the initial setup by resetting all the inputs.
resetInputs();

///////////////////////////////////////////////////////////
// BUTTON SETUP                                          //
///////////////////////////////////////////////////////////

// Setup the click handlers for the buttons where the corresponding method is needed
// at multiple places.
$("#resetInputButton").click(resetInputs);
$("#initButton").click(requestApiData);

// Handle clicks on the "back to start" button.
$("#startButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Reset the values.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Avoid leaving old output triples inside the text area.
    outputArea.text("");
    currentOutput = [];

    updateSlidingWindow(true);
    updateOutputs();
});

// Handle clicks on the "previous step" button.
$("#previousButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving backwards now.
    isForward = false;

    // Stop if it is the first step.
    if (stepNumber == 1 && stepNumberSub == 1) {
        return;
    }

    changeStep();
    updateSlidingWindow(true);
    updateOutputs();

    // Avoid wrong output being generated.
    if (!isForward && stepNumberSub == 2) {
        currentOutput.pop();
        outputArea.text(currentOutput.join(" "));
    }
});

// Handle clicks on the "next step" button.
$("#nextButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving forward now.
    isForward = true;

    changeStep();
    updateSlidingWindow(true);
    updateOutputs();
});

// Handle clicks on the "last step" button.
$("#endButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Set the end values.
    stepNumber = stepCount;
    stepNumberSub = 2;
    isForward = true;

    updateSlidingWindow(false);
    updateOutputs();

    // Make sure that all steps are displayed inside the output area.
    currentOutput = []
    for (let i = 0; i < stepCount; i++) {
        const bestMatch = steps[i]["output"];
        const triple = "(" + bestMatch["offset"] + ", " + bestMatch["length"] + ", C('" + bestMatch["character"] + "'))"
        currentOutput.push(triple);
    }
    outputArea.text(currentOutput.join(" "));
});

// Add the length indicator for the initial search buffer field.
$("#initialSearchBufferInput").maxlength({
    alwaysShow: true,
    limitReachedClass: "limitReached",
    separator: " out of ",
    preText: "You wrote ",
    postText: " characters.",
    validate: true,
});

///////////////////////////////////////////////////////////
// IMPLEMENTATION                                        //
///////////////////////////////////////////////////////////

/*
 * Reset all the input fields to their default values.
 */
function resetInputs() {
    $("#searchBufferSizeInput").val(DEFAULT_SEARCH_BUFFER_SIZE);
    $("#lookAheadBufferSizeInput").val(DEFAULT_LOOK_AHEAD_BUFFER_SIZE);
    $("#inputTextInput").val(DEFAULT_INPUT_TEXT);
    $("#initialSearchBufferInput").val(DEFAULT_INITIAL_SEARCH_BUFFER);
}

/*
 * Request the data to display from the JSON API endpoint.
 */
function requestApiData() {
    // Create the request data dictionary by reading the input fields.
    const requestData = {
        "searchBufferSize": $("#searchBufferSizeInput").val(),
        "lookAheadBufferSize": $("#lookAheadBufferSizeInput").val(),
        "inputText": $("#inputTextInput").val(),
        "initialSearchBuffer": $("#initialSearchBufferInput").val()
    };

    $.ajax({
        // As we want to send a body containing JSON data (which is more robust and
        // allows larger data chunks to be sent), the POST type has to be used -
        // although the server does not store any of the data.
        type: "POST",
        url: "/api/lz77/compress",
        // Convert the dictionary to a JSON representation.
        data: JSON.stringify(requestData),
        // Set the correct content type.
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(result) {
            // Handle errors reported by the API, for example in the case of invalid
            // data.
            if ("errorMessage" in result && result["errorMessage"] != null) {
                handleApiError(result["errorMessage"]);
                return;
            }

            // Assign the returned data to the global variables for further usage.
            jsonResponse = result;
            stepCount = jsonResponse["stepCount"];
            searchBufferSize = jsonResponse["searchBufferSize"];
            lookAheadBufferSize = jsonResponse["lookAheadBufferSize"];
            steps = jsonResponse["steps"];

            // Initialize the output.
            initializeOutput();
        },
        error: function(xhr, status, errorCode) {
            // Handle errors reported by the request itself.
            handleAjaxError(status);
        }
    });
}

/*
 * Initialize the output by clearing all output fields, creating a new sliding window
 * of the requested size and displaying the first step afterwards.
 */
function initializeOutput() {
    // Reset the step details.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Reset the output values.
    explanationValue.text("");
    outputArea.text("");
    currentOutput = [];

    // Forward the remaining tasks to dedicated methods.
    createNewSlidingWindow();
    updateOutputs();
}

/*
 * Create a new representation of the sliding window of the requested size.
 */
function createNewSlidingWindow() {
    // Fix the column spanning values.
    $("#searchBufferLabel").attr("colspan", searchBufferSize);
    $("#lookAheadBufferLabel").attr("colspan", lookAheadBufferSize);

    // Clear the content of each of the table rows for the data itself.
    const offsetLabels = $("#offsetLabels");
    offsetLabels.empty();
    const markerLocations = $("#markerLocations");
    markerLocations.empty();
    const lengthLabels = $("#lengthLabels");
    lengthLabels.empty();
    const windowContent = $("#windowContent");
    windowContent.empty();

    // Set the static properties of the search buffer.
    for (let i = searchBufferSize; i > 0; i--) {
        // First row: Offset labels.
        var row = "<td id=\"offsetLabel" + i + "\" class=\"offsetLabel\"><var>o</var> = " + i + "</td>";
        offsetLabels.append(row);

        // Second row: The content itself.
        row = "<td id=\"searchBufferOffset" + i + "\" class=\"searchBuffer\"></td>";
        windowContent.append(row);

        // Third row: Markers (arrows).
        row = "<td id=\"markerOffset" + i + "\"></td>";
        markerLocations.append(row);

        // Fourth row: Match lengths.
        row = "<td id=\"lengthLabelOffset" + i + "\"></td>";
        lengthLabels.append(row);
    }

    // Draw the buffer borders.
    $("#offsetLabel1").attr("class", "offsetLabel bufferBorder");
    $("#markerOffset1").attr("class", "bufferBorder");
    $("#lengthLabelOffset1").attr("class", "bufferBorder");
    $("#searchBufferOffset1").attr("class", "searchBuffer bufferBorder");

    // Set the static properties for the look-ahead buffer.
    for (let i = 0; i < lookAheadBufferSize; i++) {
        const row = "<td id=\"lookAheadBuffer" + i + "\" class=\"lookAheadBuffer\"></td>"
        windowContent.append(row);
    }

    // Draw the first step into the sliding window.
    updateSlidingWindow(true);
}

/*
 * Update the sliding window by first clearing all existing data and then adding the
 * new one.
 *
 * @param highlightMatches Boolean variable to indicate whether the matches should be
 *                         highlighted when drawing.
 */
function updateSlidingWindow(highlightMatches) {
    // Convert the step number into an array index for the steps array.
    const stepIndex = (stepNumberSub == 1) ? (stepNumber - 1) : stepNumber;

    // Boolean expression to ensure that the index does not go out of range.
    const isEnd = (stepIndex == stepCount);

    // Select the current step.
    const step = isEnd ? [] : steps[stepIndex];

    // Retrieve the content of the search buffer.
    var searchBufferContent = step["searchBuffer"];
    if (isEnd) {
        // Get the last "search buffer size" characters from the input.
        // The given `substr` parameter is the start index.
        const text = $("#initialSearchBufferInput").val() + $("#inputTextInput").val();
        // Make sure that too short inputs are handled correctly.
        if (text.length < searchBufferSize) {
            searchBufferContent = text;
        } else {
            searchBufferContent = text.substr(text.length - searchBufferSize);
        }
    }
    const searchBufferContentLength = searchBufferContent.length;

    // Display the search buffer.
    for (let i = searchBufferContentLength - 1; i >= 0; i--) {
        const tdId = "#searchBufferOffset" + (searchBufferContentLength - i);
        $(tdId).html(searchBufferContent.charAt(i).replace(" ", "&blank;"));
    }

    // Remove the old content from unused fields at the beginning of the buffer.
    for (let i = searchBufferSize; i > searchBufferContentLength; i--) {
        const tdId = "#searchBufferOffset" + i;
        $(tdId).text("");
    }

    // Retrieve the content of the look-ahead buffer.
    // It is empty at the end.
    const lookAheadBufferContent = isEnd ? "" : step["lookAheadBuffer"];
    const lookAheadBufferContentLength = lookAheadBufferContent.length;

    // Display the look-ahead buffer.
    for (let i = 0; i < lookAheadBufferContentLength; i++) {
        const tdId = "#lookAheadBuffer" + i;
        $(tdId).html(lookAheadBufferContent.charAt(i).replace(" ", "&blank;"));
    }

    // Remove the old content from unused fields at the end of the buffer.
    for (let i = lookAheadBufferContentLength; i < lookAheadBufferSize; i++) {
        const tdId = "#lookAheadBuffer" + i;
        $(tdId).text("");
    }

    // Remove all match indicators.
    $("#windowContent").children("td").each(function() {
        // Remove the `matchCell` class from the CSS class list.
        $(this).attr("class", $(this).attr("class").replace("matchCell", ""));
    });
    $("#markerLocations").children("td").each(function() {
        $(this).html("");
    });
    $("#lengthLabels").children("td").each(function() {
        $(this).html("");
    });

    // If no matches should be highlighted, there is nothing more to do here.
    // Avoid highlighting anything for the window movements as well.
    if (!highlightMatches || stepNumberSub == 2) {
        return;
    }

    // Stop here if there are no steps.
    if (stepCount === 0) {
        return;
    }

    // Retrieve the matches.
    const matches = step["matches"];
    const matchesCount = matches ? matches.length : -1;

    // We have finished processing if there is no match.
    if (matchesCount < 1) {
        $("#lookAheadBuffer0").attr("class", "lookAheadBuffer matchCell");
        return;
    }

    // Mark all matches with an arrow and their length.
    for (let i = 0; i < matchesCount; i++) {
        const match = matches[i];
        const offset = match["offset"];
        $("#markerOffset" + offset).html("<i class=\"fas fa-arrow-up\"></i>");
        $("#lengthLabelOffset" + offset).html("<var>&ell;</var> = " + match["length"]);
    }

    // Retrieve the values for the best match.
    const bestMatch = step["output"];
    const bestMatchLength = bestMatch["length"];
    const bestMatchOffset = bestMatch["offset"];

    // Highlight the cells corresponding to the best match in both buffers.
    for (let i = 0; i < bestMatchLength; i++) {
        var tdId = "#searchBufferOffset" + (bestMatchOffset - i);
        $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
        tdId = "#lookAheadBuffer" + i;
        $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
    }

    // Use a circled arrow for the best match.
    $("#markerOffset" + bestMatchOffset).html("<i class=\"fas fa-arrow-circle-up\"></i>");
}

/*
 * Update the output fields.
 */
function updateOutputs() {
    // Set the step number.
    stepNumberValue.text("" + stepNumber + "." + stepNumberSub + " out of " + stepCount + ".2");

    // Retrieve the step to show.
    const step = steps[stepNumber - 1];

    // There is nothing to show as the input is empty.
    if (!step && stepCount === 0) {
        explanationValue.html("The input is empty, so there are no steps to show.");
        stepNumberValue.text("0 ouf of 0");
        return;
    }

    // Retrieve the best match and the used strategy.
    const bestMatch = step["output"];
    const bestMatchLength = bestMatch["length"];
    const bestMatchOffset = bestMatch["offset"];
    const strategy = step["strategy"];

    // For sliding window movements, we can stop after the explanation.
    if (stepNumberSub == 2) {
        const length1Subject = ((bestMatchLength + 1) == 1) ? "character" : "characters";
        var explanation = "We are moving the sliding window " + (bestMatchLength + 1) + " " + length1Subject + " to the left as the best match had a length of <var>&ell;</var> = " + bestMatchLength + " and we have to consider the third triple element as well.";
        if (strategy === "lastCharacter") {
            explanation += " The look-ahead buffer is empty now, so we can stop.";
        }
        if (step["shortenedLastMatch"]) {
            explanation += " Remember that we had to reduce the match length by 1 for the output as the third triple element would have been empty otherwise.";
        }
        explanationValue.html(explanation);
        return;
    }

    // Some encoding happened. Map the texts to the corresponding strategy tags.
    var explanation = null;
    switch (strategy) {
        case "noMatch":
            explanation = "We cannot find a match inside the search buffer and therefore just";
            break;
        case "onlyMatch":
            explanation = "We have only one match at <var>o</var> = " + bestMatchOffset + " and therefore";
            break;
        case "longestMatch":
            explanation = "We choose the longest match at <var>o</var> = " + bestMatchOffset + " of length <var>&ell;</var> = " + bestMatchLength + " and therefore";
            break;
        case "smallestOffset":
            explanation = "We choose one of the longest matches of length <var>&ell;</var> = " + bestMatchLength + " where the offset <var>o</var> = " + bestMatchOffset + " is the smallest one and therefore";
            break;
        case "lastCharacter":
            explanation = "We only have one character left, but always have to emit one character as the third triple element. For this reason we";
            break;
        default:
            handleApiError("Unknown strategy for choosing the best match.");
            break;
    }

    // Create the output triple.
    const triple = "(" + bestMatchOffset + ", " + bestMatchLength + ", C('" + bestMatch["character"] + "'))"

    // Add the output triple to the explanation text.
    explanation += " output <code>" + triple + "</code>."

    if (step["shortenedLastMatch"]) {
        explanation += " The match length has to be reduced by 1 as the third triple element would be empty otherwise.";
    }

    // Display the explanation text.
    explanationValue.html(explanation);

    // Only add the triple to the output if we are moving forward as in the other case
    // we always have to remove a triple instead.
    if (isForward) {
        currentOutput.push(triple);
        outputArea.text(currentOutput.join(" "));
    }
}
