/*
 * Simulation of data decompression with LZ77 using a JSON API, client-side.
 *
 * This file contains the client-side code for communicating with the JSON API for
 * simulating/visualizing the LZ77 algorithm as per [Say17] using a stepwise approach.
 */

///////////////////////////////////////////////////////////
// CONFIGURATION                                         //
///////////////////////////////////////////////////////////

// Define the default values for the input section.
DEFAULT_SEARCH_BUFFER_SIZE = 10;
DEFAULT_LOOK_AHEAD_BUFFER_SIZE = 10;
DEFAULT_COMPRESSED_DATA = "(2, 1, C('o')) (0, 0, C('r')) (0, 0, C('e')) (0, 0, C('m')) (0, 0, C(' ')) (0, 0, C('i')) (0, 0, C('p')) (0, 0, C('s')) (0, 0, C('u')) (0, 0, C('m'))";
DEFAULT_RECENTLY_COMPRESSED = "NULL;";

// Get the output text elements as they are referenced quite often and do not change
// their IDs.
const currentTripleValue = $("#currentTripleValue");

// Some global variables which hold the most often used fields of the JSON response.
var searchBufferSize = -1;
var lookAheadBufferSize = -1;

// The current output. We are storing this as an array to allow removing
// elements without any hassle for the backwards version.
var currentOutput = [];

///////////////////////////////////////////////////////////
// SETUP                                                 //
///////////////////////////////////////////////////////////

// Perform the initial setup by resetting all the inputs.
resetInputs();

///////////////////////////////////////////////////////////
// BUTTON SETUP                                          //
///////////////////////////////////////////////////////////

// Setup the click handlers for the buttons where the corresponding method is needed
// at multiple places.
$("#resetInputButton").click(resetInputs);
$("#initButton").click(requestApiData);

// Handle clicks on the "back to start" button.
$("#startButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Reset the values.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Avoid leaving old output text inside the text area.
    outputArea.text("");
    currentOutput = [];

    updateSlidingWindow(true);
    updateOutputs();
});

// Handle clicks on the "previous step" button.
$("#previousButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving backwards now.
    isForward = false;

    // Stop if it is the first step.
    if (stepNumber == 1 && stepNumberSub == 1) {
        return;
    }

    changeStep();
    updateSlidingWindow(true);
    updateOutputs();

    // Avoid wrong output being generated.
    if (!isForward && stepNumberSub == 2) {
        currentOutput.pop();
        outputArea.text(currentOutput.join(""));
    }
});

// Handle clicks on the "next step" button.
$("#nextButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving forward now.
    isForward = true;

    changeStep();
    updateSlidingWindow(true);
    updateOutputs();
});

// Handle clicks on the "last step" button.
$("#endButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Set the end values.
    stepNumber = stepCount;
    stepNumberSub = 2;
    isForward = true;

    updateSlidingWindow(false);
    updateOutputs();

    // Make sure that all steps are displayed inside the output area.
    currentOutput = []
    for (let i = 0; i < stepCount; i++) {
        currentOutput.push(steps[i]["newCharacters"]);
    }
    outputArea.text(currentOutput.join(""));
});

// Add the length indicator for the initial search buffer field.
$("#recentlyDecompressedInput").maxlength({
    alwaysShow: true,
    limitReachedClass: "limitReached",
    separator: " out of ",
    preText: "You wrote ",
    postText: " characters.",
    validate: true,
});

///////////////////////////////////////////////////////////
// IMPLEMENTATION                                        //
///////////////////////////////////////////////////////////

/*
 * Reset all the input fields to their default values.
 */
function resetInputs() {
    $("#searchBufferSizeInput").val(DEFAULT_SEARCH_BUFFER_SIZE);
    $("#lookAheadBufferSizeInput").val(DEFAULT_LOOK_AHEAD_BUFFER_SIZE);
    $("#compressedDataInput").val(DEFAULT_COMPRESSED_DATA);
    $("#recentlyDecompressedInput").val(DEFAULT_RECENTLY_COMPRESSED);
}

/*
 * Request the data to display from the JSON API endpoint.
 */
function requestApiData() {
    // Create the request data dictionary by reading the input fields.
    const requestData = {
        "searchBufferSize": $("#searchBufferSizeInput").val(),
        "lookAheadBufferSize": $("#lookAheadBufferSizeInput").val(),
        "compressedData": $("#compressedDataInput").val(),
        "recentlyDecompressed": $("#recentlyDecompressedInput").val()
    };

    $.ajax({
        // As we want to send a body containing JSON data (which is more robust and
        // allows larger data chunks to be sent), the POST type has to be used -
        // although the server does not store any of the data.
        type: "POST",
        url: "/api/lz77/decompress",
        // Convert the dictionary to a JSON representation.
        data: JSON.stringify(requestData),
        // Set the correct content type.
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(result) {
            // Handle errors reported by the API, for example in the case of invalid
            // data.
            if ("errorMessage" in result && result["errorMessage"] != null) {
                handleApiError(result["errorMessage"]);
                return;
            }

            // Assign the returned data to the global variables for further usage.
            jsonResponse = result;
            stepCount = jsonResponse["stepCount"];
            searchBufferSize = jsonResponse["searchBufferSize"];
            lookAheadBufferSize = jsonResponse["lookAheadBufferSize"];
            steps = jsonResponse["steps"];

            // Initialize the output.
            initializeOutput();
        },
        error: function(xhr, status, errorCode) {
            // Handle errors reported by the request itself.
            handleAjaxError(status);
        }
    });
}

/*
 * Initialize the output by clearing all output fields, creating a new sliding window
 * of the requested size and displaying the first step afterwards.
 */
function initializeOutput() {
    // Reset the step details.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Reset the output values.
    explanationValue.text("");
    outputArea.text("");
    currentTripleValue.text("");
    currentOutput = [];

    // Forward the remaining tasks to dedicated methods.
    createNewSlidingWindow();
    updateOutputs();
}

/*
 * Create a new representation of the sliding window of the requested size.
 */
function createNewSlidingWindow() {
    // Fix the column spanning values.
    $("#rdLabel").attr("colspan", searchBufferSize);
    $("#overflowLabel").attr("colspan", lookAheadBufferSize);

    // Clear the content of each of the table rows for the data itself.
    const offsetLabels = $("#offsetLabels");
    offsetLabels.empty();
    const windowContent = $("#windowContent");
    windowContent.empty();

    // Set the static properties of the search buffer (recently decompressed data).
    for (let i = searchBufferSize; i > 0; i--) {
        // First row: Offset labels.
        var row = "<td id=\"offsetLabel" + i + "\" class=\"offsetLabel\"><var>o</var> = " + i + "</td>";
        offsetLabels.append(row);

        // Second row: The content itself.
        row = "<td id=\"recentlyDecompressedOffset" + i + "\" class=\"searchBuffer\"></td>";
        windowContent.append(row);
    }

    // Draw the buffer borders.
    $("#offsetLabel1").attr("class", "offsetLabel bufferBorder");
    $("#recentlyDecompressedOffset1").attr("class", "searchBuffer bufferBorder");

    // Set the static properties for the look-ahead buffer (overflow).
    for (let i = 0; i < lookAheadBufferSize; i++) {
        const row = "<td id=\"overflow" + i + "\" class=\"lookAheadBuffer\"></td>"
        windowContent.append(row);
    }

    // Draw the first step into the sliding window.
    updateSlidingWindow(true);
}

/*
 * Update the sliding window by first clearing all existing data and then adding the
 * new one.
 *
 * @param highlightMatches Boolean variable to indicate whether the matches should be
 *                         highlighted when drawing.
 */
function updateSlidingWindow(highlightMatches) {
    // Convert the step number into an array index for the steps array.
    const stepIndex = (stepNumberSub == 1) ? (stepNumber - 1) : stepNumber;

    // Boolean expression to ensure that the index does not go out of range.
    const isEnd = (stepIndex == stepCount);

    // Select the current step.
    const step = isEnd ? [] : steps[stepIndex];

    // Retrieve the content of the search buffer.
    var recentlyDecompressedData = step["recentlyDecompressed"];
    if (isEnd) {
        // Get the last "search buffer size" characters from the JSON data.
        // The given `substr` parameter is the start index.
        const previousStep = steps[stepIndex - 1];
        var text = "";
        if (previousStep) {
            text = previousStep["recentlyDecompressed"] + previousStep["newCharacters"];
        }
        // Make sure that too short inputs are handled correctly.
        if (text.length < searchBufferSize) {
            recentlyDecompressedData = text;
        } else {
            recentlyDecompressedData = text.substr(text.length - searchBufferSize);
        }
    }
    const recentlyDecompressedLength = recentlyDecompressedData.length;

    // Display the search buffer (recently decompressed data).
    for (let i = recentlyDecompressedLength - 1; i >= 0; i--) {
        const tdId = "#recentlyDecompressedOffset" + (recentlyDecompressedLength - i);
        $(tdId).html(recentlyDecompressedData.charAt(i).replace(" ", "&blank;"));
    }

    // Remove the old content from unused fields at the beginning of the buffer.
    for (let i = searchBufferSize; i > recentlyDecompressedLength; i--) {
        const tdId = "#recentlyDecompressedOffset" + i;
        $(tdId).text("");
    }

    // Retrieve the content of the look-ahead buffer (overflow data).
    // It is empty at the end and might be empty in-between as well.
    var overflowContent;
    if (isEnd || step["overflow"] == null || stepNumberSub == 2) {
        overflowContent = "";
    } else {
        overflowContent = step["overflow"];
    }
    const overflowContentLength = overflowContent.length;

    // Display the overflow content.
    for (let i = 0; i < overflowContentLength; i++) {
        const tdId = "#overflow" + i;
        $(tdId).html(overflowContent.charAt(i).replace(" ", "&blank;"));
    }

    // Remove the old content from unused fields at the end of the buffer.
    for (let i = overflowContentLength; i < lookAheadBufferSize; i++) {
        const tdId = "#overflow" + i;
        $(tdId).text("");
    }

    // Remove all match indicators.
    $("#windowContent").children("td").each(function() {
        // Remove the `matchCell` class from the CSS class list.
        $(this).attr("class", $(this).attr("class").replace("matchCell", ""));
    });

    // If no matches should be highlighted, there is nothing more to do here.
    // Avoid highlighting anything for the window movements as well.
    if (!highlightMatches || stepNumberSub == 2) {
        return;
    }

    // Stop here if there are no steps.
    if (stepCount === 0) {
        return;
    }

    // Retrieve the triple.
    const triple = step["triple"];
    const tripleOffset = triple["offset"];
    const rdLength = triple["length"] - overflowContentLength;

    // We have finished processing if there is not match.
    if (tripleOffset === 0) {
        return;
    }

    // Highlight the cells corresponding to the triple.
    for (let i = 0; i < rdLength; i++) {
        var tdId = "#recentlyDecompressedOffset" + (tripleOffset - i);
        $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
    }
    for (let i = 0; i < overflowContentLength; i++) {
        var tdId = "#overflow" + i;
        $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
    }
}

/*
 * Update the output fields.
 */
function updateOutputs() {
    // Set the step number.
    stepNumberValue.text("" + stepNumber + "." + stepNumberSub + " out of " + stepCount + ".2");

    // Retrieve the step to show.
    const step = steps[stepNumber - 1];

    // There is nothing to show as the input is empty.
    if (!step && stepCount === 0) {
        explanationValue.html("The input is empty, so there are no steps to show.");
        stepNumberValue.text("0 ouf of 0");
        return;
    }

    // Retrieve the triple and the used strategy.
    const triple = step["triple"];
    const tripleOffset = triple["offset"];
    const tripleLength = triple["length"];
    const tripleCharacter = triple["character"].replace(" ", "&blank;");
    const strategy = step["strategy"];

    // Set the current triple.
    currentTripleValue.html("(<var>o</var>, <var>&ell;</var>, <var>c</var>) = (" + tripleOffset + ", " + tripleLength + ", <code>'" + tripleCharacter + "'</code>)");

    // For sliding window movements, we can stop after the explanation.
    if (stepNumberSub == 2) {
        const length1Subject = ((tripleLength + 1) == 1) ? "character" : "characters";
        var explanation = "We are moving the sliding window " + (tripleLength + 1) + " " + length1Subject + " to the left as the triple had a length of <var>&ell;</var> = " + tripleLength + " and we have to consider the third triple element <var>c</var> = <code>'" + tripleCharacter + "'</code> as well.";
        explanationValue.html(explanation);
        return;
    }

    // Map the texts to the corresponding strategy tags.
    var explanation = null;
    switch (strategy) {
        case "appendOnly":
            explanation = "The offset is <var>o</var> = 0 which indicates that there is not a match inside the recently decompressed data.";
            break;
        case "simpleCopy":
            explanation = "The offset <var>o</var> = " + tripleOffset + " is smaller than the length <var>&ell;</var> = " + tripleLength + " which indicates that we can just copy the " + tripleLength + " values starting at the offset " + tripleOffset + ".";
            break;
        case "continuePattern":
            explanation = "The offset <var>o</var> = " + tripleOffset + " is greater than the length <var>&ell;</var> = " + tripleLength + " which indicates that we have to copy " + step["overflow"].length + " values starting at the offset " + tripleOffset + " by repeating the given values.";
            break;
        default:
            handleApiError("Unknown strategy for handling the triple.");
            break;
    }

    // Get the newly added characters.
    const newCharacters = step["newCharacters"];

    // Add the new characters to the explanation text and display the text.
    explanation += " We add the string <code>'" + newCharacters + "'</code> to the output.";
    explanationValue.html(explanation);

    // Only add the new characters to the output if we are moving forward as in the
    // other case we always have to remove this output instead.
    if (isForward) {
        currentOutput.push(newCharacters);
        outputArea.text(currentOutput.join(""));
    }
}
