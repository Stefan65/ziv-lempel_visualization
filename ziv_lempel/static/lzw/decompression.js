/*
 * Simulation of data decompression with LZW using a JSON API, client-side.
 *
 * This file contains the client-side code for communicating with the JSON API for
 * simulating/visualizing the LZW algorithm as per [Say17] using a stepwise approach.
 */

///////////////////////////////////////////////////////////
// CONFIGURATION                                         //
///////////////////////////////////////////////////////////

// Define the default values for the input section.
DEFAULT_COMPRESSED_DATA = "4 5 3 1 2 8 2 7 9 7 4";
DEFAULT_INITIAL_DICTIONARY = "S ITH";

// Get the output text elements as they are referenced quite often and do not change
// their IDs.
const currentIndexValue = $("#currentIndexValue");

// The current output data. We are storing this as an array to allow removing
// elements without any hassle for the backwards version.
var currentOutput = [];

// The size of the initial dictionary to avoid removing to many rows.
var initialDictionarySize = -1;

///////////////////////////////////////////////////////////
// SETUP                                                 //
///////////////////////////////////////////////////////////

// Perform the initial setup by resetting all the inputs.
resetInputs();

///////////////////////////////////////////////////////////
// BUTTON SETUP                                          //
///////////////////////////////////////////////////////////

// Setup the click handlers for the buttons where the corresponding method is needed
// at multiple places.
$("#resetInputButton").click(resetInputs);
$("#initButton").click(requestApiData);

// Handle clicks on the "back to start" button.
$("#startButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Reset the values.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Avoid leaving old output data inside the text area.
    outputArea.text("");
    currentOutput = [];

    createNewDictionary();
    updateDictionary(true);
    updateOutputs();
});

// Handle clicks on the "previous step" button.
$("#previousButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving backwards now.
    isForward = false;

    // Stop if it is the first step.
    if (stepNumber == 1 && stepNumberSub == 1) {
        return;
    }

    changeStep();
    updateOutputs();

    // Remove the last row for backwards operations if needed.
    // To avoid removing the initial dictionary, we have to check the number of
    // children as well.
    childCount = $("#dictionaryValues").children().length;
    if (!isForward && stepNumberSub == 1 && childCount > initialDictionarySize) {
        $("#dictionaryValues tr:last").remove();
    } else if (isForward || stepNumberSub == 2) {
        updateDictionary(true);
    }

    // Avoid wrong output being generated.
    if (!isForward && stepNumberSub == 2) {
        currentOutput.pop();
        outputArea.text(currentOutput.join(""));
    }
});

// Handle clicks on the "next step" button.
$("#nextButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving forward now.
    isForward = true;

    changeStep();
    updateDictionary(true);
    updateOutputs();
});

// Handle clicks on the "last step" button.
$("#endButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Add all missing entries to the dictionary.
    isForward = true;
    while (stepNumber < stepCount || stepNumberSub != 2) {
        changeStep();
        updateDictionary(false);
    }

    // Set the end values.
    stepNumber = stepCount;
    stepNumberSub = 2;

    updateOutputs();

    // Make sure that all steps are displayed inside the output area.
    currentOutput = []
    for (let i = 0; i < stepCount; i++) {
        currentOutput.push(steps[i]["newCharacters"]);
    }
    outputArea.text(currentOutput.join(""));
});

///////////////////////////////////////////////////////////
// IMPLEMENTATION                                        //
///////////////////////////////////////////////////////////

/*
 * Reset all the input fields to their default values.
 */
function resetInputs() {
    $("#compressedDataInput").val(DEFAULT_COMPRESSED_DATA);
    $("#initialDictionaryInput").val(DEFAULT_INITIAL_DICTIONARY);
}

/*
 * Request the data to display from the JSON API endpoint.
 */
function requestApiData() {
    // Create the request data dictionary by reading the input fields.
    const requestData = {
        "compressedData": $("#compressedDataInput").val(),
        "initialDictionary": $("#initialDictionaryInput").val()
    };

    $.ajax({
        // As we want to send a body containing JSON data (which is more robust and
        // allows larger data chunks to be sent), the POST type has to be used -
        // although the server does not store any of the data.
        type: "POST",
        url: "/api/lzw/decompress",
        // Convert the dictionary to a JSON representation.
        data: JSON.stringify(requestData),
        // Set the correct content type.
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(result) {
            // Handle errors reported by the API, for example in the case of invalid
            // data.
            if ("errorMessage" in result && result["errorMessage"] != null) {
                handleApiError(result["errorMessage"]);
                return;
            }

            // Assign the returned data to the global variables for further usage.
            jsonResponse = result;
            stepCount = jsonResponse["stepCount"];
            steps = jsonResponse["steps"];

            // Initialize the output.
            initializeOutput();
        },
        error: function(xhr, status, errorCode) {
            // Handle errors reported by the request itself.
            handleAjaxError(status);
        }
    });
}

/*
 * Initialize the output by clearing all output fields, creating a new sliding window
 * of the requested size and displaying the first step afterwards.
 */
function initializeOutput() {
    // Reset the step details.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Reset the output values.
    explanationValue.text("");
    outputArea.text("");
    currentOutput = [];

    // Forward the remaining tasks to dedicated methods.
    createNewDictionary();
    updateOutputs();
}

/*
 * Create a new dictionary.
 */
function createNewDictionary() {
    // Clear the content of each of the table rows for the data itself.
    const dictionaryValues = $("#dictionaryValues");
    dictionaryValues.empty();

    // Retrieve the initial dictionary data.
    const initialDictionary = jsonResponse["initialDictionary"];
    initialDictionarySize = Object.keys(initialDictionary).length;
    for (let index in initialDictionary) {
        entry = initialDictionary[index];
        const newRow = $("<tr id=\"dictionaryRow" + index + "\">");
        newRow.append("<td id=\"dictionaryIndex" + index + "\" class=\"\">" + index + "</td>");
        newRow.append("<td id=\"dictionaryEntry" + index + "\" class=\"dictionaryEntry\">" + entry.replace(/ /g, "&blank;") + "</td>");
        dictionaryValues.append(newRow);
    }

    // Draw the first step.
    updateDictionary(true);
}

/*
 * Update the dictionary by first clearing all existing data and then adding the new one.
 *
 * @param highlightMatches Boolean variable to indicate whether the matches should be
 *                         highlighted when drawing. Gets ignored at the moment.
 */
function updateDictionary(highlightMatches) {
    // Convert the step number into an array index for the steps array.
    const stepIndex = stepNumber - 1;

    // Boolean expression to ensure that the index does not go out of range.
    const isEnd = (stepIndex == stepCount);

    // Select the current step.
    const step = isEnd ? [] : steps[stepIndex];

    // Retrieve the dictionary value item.
    const dictionaryValues = $("#dictionaryValues");

    // Add a new row in the second sub-step.
    if (stepNumberSub == 2 && !isEnd && isForward) {
        const dictionaryUpdate = step["dictionaryUpdate"];
        // Avoid "undefined" errors.
        if (dictionaryUpdate) {
            const newIndex = dictionaryUpdate["index"];
            const newEntry = dictionaryUpdate["part1"].replace(/ /g, "&blank;") + " ... " + dictionaryUpdate["part2"].replace(/ /g, "&blank;");

            const newRow = $("<tr id=\"dictionaryRow" + newIndex + "\">");
            newRow.append("<td id=\"dictionaryIndex" + newIndex + "\" class=\"\">" + newIndex + "</td>");
            newRow.append("<td id=\"dictionaryEntry" + newIndex + "\" class=\"dictionaryEntry\">" + newEntry + "</td>");
            dictionaryValues.append(newRow);
        }
    }

    // Remove all match indicators.
    const dictionaryRows = dictionaryValues.children("tr");
    const dictionaryRowCount = dictionaryRows.length;
    for (let i = 0; i < dictionaryRowCount; i++) {
        dictionaryRows.eq(i).children("td").each(function() {
            // Remove the `matchCell` class from the CSS class list.
            $(this).attr("class", $(this).attr("class").replace("matchCell", ""));
        });
    }

    // Stop here if there are no steps.
    if (stepCount === 0) {
        return;
    }

    // Retrieve the match data.
    const matchIndex = step["index"];

    // Highlight the row with the match.
    var tdId = "#dictionaryIndex" + matchIndex;
    $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
    tdId = "#dictionaryEntry" + matchIndex;
    $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
}

/*
 * Update the output fields.
 */
function updateOutputs() {
    // Set the step number.
    stepNumberValue.text("" + stepNumber + "." + stepNumberSub + " out of " + stepCount + ".2");

    // Retrieve the step to show.
    const step = steps[stepNumber - 1];

    // There is nothing to show as the input is empty.
    if (!step && stepCount === 0) {
        explanationValue.html("The input is empty, so there are no steps to show.");
        stepNumberValue.text("0 ouf of 0");
        return;
    }

    // Retrieve the match and the used strategy.
    const matchIndex = step["index"];

    // Set the current index.
    currentIndexValue.html("<var>i</var> = " + matchIndex);

    // For dictionary updates, we can stop after the explanation.
    if (stepNumberSub == 2) {
        const dictionaryUpdate = step["dictionaryUpdate"];
        if (!dictionaryUpdate) {
            explanation = "We are not performing a dictionary update as the string is already part of the dictionary.";
        } else {
            const entry = dictionaryUpdate["part1"] + dictionaryUpdate["part2"];
            explanation = "We are adding the new entry <code>'" + entry + "'</code> at index <var>i</var> = " + dictionaryUpdate["index"] + " which is equal to the recently added output characters.";
        }
        explanationValue.html(explanation);
        return;
    }

    // Some encoding happened.
    const newCharacters = step["newCharacters"];
    var explanation = null;
    if (step["strategy"] == "normalReference") {
        explanation = "We have a normal reference to the already existing dictionary entry at index <var>i</var> = " + matchIndex + " and therefore add <code>'" + newCharacters + "'</code> to the output.";
    } else {
        explanation = "We are referencing the entry at index <var>i</var> = " + matchIndex + " which is not part of the dictionary. For this reason we use the dictionary entry created in the previous step and concatenate it with its first character. Now we can add <code>'" + newCharacters + "'</code> to the output.";
    }

    explanationValue.html(explanation);

    // Only add the new characters to the output if we are moving forward as in the
    // other case we always have to remove this output instead.
    if (isForward) {
        currentOutput.push(newCharacters);
        outputArea.text(currentOutput.join(""));
    }
}
