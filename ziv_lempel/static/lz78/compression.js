/*
 * Simulation of data compression with LZ78 using a JSON API, client-side.
 *
 * This file contains the client-side code for communicating with the JSON API for
 * simulating/visualizing the LZ78 algorithm as per [Say17] using a stepwise approach.
 */

///////////////////////////////////////////////////////////
// CONFIGURATION                                         //
///////////////////////////////////////////////////////////

// Define the default values for the input section.
DEFAULT_INPUT_TEXT = "ASCII ANSI";

// The current output couples. We are storing this as an array to allow removing
// elements without any hassle for the backwards version.
var currentOutput = [];

///////////////////////////////////////////////////////////
// SETUP                                                 //
///////////////////////////////////////////////////////////

// Perform the initial setup by resetting all the inputs.
resetInputs();

///////////////////////////////////////////////////////////
// BUTTON SETUP                                          //
///////////////////////////////////////////////////////////

// Setup the click handlers for the buttons where the corresponding method is needed
// at multiple places.
$("#resetInputButton").click(resetInputs);
$("#initButton").click(requestApiData);

// Handle clicks on the "back to start" button.
$("#startButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Reset the values.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Avoid leaving old output couples inside the text area.
    outputArea.text("");
    currentOutput = [];

    createNewDictionary();
    updateDictionary(true);
    updateOutputs();
});

// Handle clicks on the "previous step" button.
$("#previousButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving backwards now.
    isForward = false;

    // Stop if it is the first step.
    if (stepNumber == 1 && stepNumberSub == 1) {
        return;
    }

    changeStep();
    updateOutputs();

    // Remove the last row for backwards operations if needed.
    if (!isForward && stepNumberSub == 1) {
        $("#dictionaryValues tr:last").remove();
    } else {
        updateDictionary(true);
    }

    // Avoid wrong output being generated.
    if (!isForward && stepNumberSub == 2) {
        currentOutput.pop();
        outputArea.text(currentOutput.join(" "));
    }
});

// Handle clicks on the "next step" button.
$("#nextButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // We are moving forward now.
    isForward = true;

    changeStep();
    updateDictionary(true);
    updateOutputs();
});

// Handle clicks on the "last step" button.
$("#endButton").click(function() {
    // Avoid errors if no data is available.
    if (steps == null) {
        return;
    }

    // Add all missing entries to the dictionary.
    isForward = true;
    while (stepNumber < stepCount || stepNumberSub != 2) {
        changeStep();
        updateDictionary(false);
    }

    // Set the end values.
    stepNumber = stepCount;
    stepNumberSub = 2;

    updateOutputs();

    // Make sure that all steps are displayed inside the output area.
    currentOutput = []
    for (let i = 0; i < stepCount; i++) {
        const match = steps[i]["output"];
        const couple = "(" + match["index"] + ", C('" + match["character"] + "'))"
        currentOutput.push(couple);
    }
    outputArea.text(currentOutput.join(" "));
});

///////////////////////////////////////////////////////////
// IMPLEMENTATION                                        //
///////////////////////////////////////////////////////////

/*
 * Reset all the input fields to their default values.
 */
function resetInputs() {
    $("#inputTextInput").val(DEFAULT_INPUT_TEXT);
}

/*
 * Request the data to display from the JSON API endpoint.
 */
function requestApiData() {
    // Create the request data dictionary by reading the input fields.
    const requestData = {
        "inputText": $("#inputTextInput").val()
    };

    $.ajax({
        // As we want to send a body containing JSON data (which is more robust and
        // allows larger data chunks to be sent), the POST type has to be used -
        // although the server does not store any of the data.
        type: "POST",
        url: "/api/lz78/compress",
        // Convert the dictionary to a JSON representation.
        data: JSON.stringify(requestData),
        // Set the correct content type.
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(result) {
            // Handle errors reported by the API, for example in the case of invalid
            // data.
            if ("errorMessage" in result && result["errorMessage"] != null) {
                handleApiError(result["errorMessage"]);
                return;
            }

            // Assign the returned data to the global variables for further usage.
            jsonResponse = result;
            stepCount = jsonResponse["stepCount"];
            steps = jsonResponse["steps"];

            // Initialize the output.
            initializeOutput();
        },
        error: function(xhr, status, errorCode) {
            // Handle errors reported by the request itself.
            handleAjaxError(status);
        }
    });
}

/*
 * Initialize the output by clearing all output fields, creating a new sliding window
 * of the requested size and displaying the first step afterwards.
 */
function initializeOutput() {
    // Reset the step details.
    stepNumber = 1;
    stepNumberSub = 1;
    isForward = true;

    // Reset the output values.
    explanationValue.text("");
    outputArea.text("");
    currentOutput = [];

    // Forward the remaining tasks to dedicated methods.
    createNewDictionary();
    updateOutputs();
}

/*
 * Create a new dictionary.
 */
function createNewDictionary() {
    // Clear the content of each of the table rows for the data itself.
    const dictionaryValues = $("#dictionaryValues");
    dictionaryValues.empty();

    // Draw the first step.
    updateDictionary(true);
}

/*
 * Update the dictionary by first clearing all existing data and then adding the new one.
 *
 * @param highlightMatches Boolean variable to indicate whether the matches should be
 *                         highlighted when drawing. Gets ignored at the moment.
 */
function updateDictionary(highlightMatches) {
    // Convert the step number into an array index for the steps array.
    const stepIndex = stepNumber - 1;

    // Boolean expression to ensure that the index does not go out of range.
    const isEnd = (stepIndex == stepCount);

    // Select the current step.
    const step = isEnd ? [] : steps[stepIndex];

    // Retrieve the dictionary value item.
    const dictionaryValues = $("#dictionaryValues");

    // Add a new row in the second sub-step.
    if (stepNumberSub == 2 && !isEnd) {
        const dictionaryUpdate = step["dictionaryUpdate"];
        // Avoid "undefined" errors.
        if (dictionaryUpdate) {
            const newIndex = dictionaryUpdate["index"];
            const newEntry = dictionaryUpdate["entry"];

            const newRow = $("<tr id=\"dictionaryRow" + newIndex + "\">");
            newRow.append("<td id=\"dictionaryIndex" + newIndex + "\" class=\"\">" + newIndex + "</td>");
            newRow.append("<td id=\"dictionaryEntry" + newIndex + "\" class=\"dictionaryEntry\">" + newEntry.replace(/ /g, "&blank;") + "</td>");
            dictionaryValues.append(newRow);
        }
    }

    // Remove all match indicators.
    const dictionaryRows = dictionaryValues.children("tr");
    const dictionaryRowCount = dictionaryRows.length;
    for (let i = 0; i < dictionaryRowCount; i++) {
        dictionaryRows.eq(i).children("td").each(function() {
            // Remove the `matchCell` class from the CSS class list.
            $(this).attr("class", $(this).attr("class").replace("matchCell", ""));
        });
    }

    // Stop here if there are no steps.
    if (stepCount === 0) {
        return;
    }

    // Retrieve the match data.
    const matchIndex = step["output"]["index"];

    // Stop if there is no match.
    if (matchIndex === 0) {
        return;
    }

    // Highlight the row with the match.
    var tdId = "#dictionaryIndex" + matchIndex;
    $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
    tdId = "#dictionaryEntry" + matchIndex;
    $(tdId).attr("class", $(tdId).attr("class") + " matchCell");
}

/*
 * Update the output fields.
 */
function updateOutputs() {
    // Set the step number.
    stepNumberValue.text("" + stepNumber + "." + stepNumberSub + " out of " + stepCount + ".2");

    // Retrieve the step to show.
    const step = steps[stepNumber - 1];

    // There is nothing to show as the input is empty.
    if (!step && stepCount === 0) {
        explanationValue.html("The input is empty, so there are no steps to show.");
        stepNumberValue.text("0 ouf of 0");
        return;
    }

    // Retrieve the match and the used strategy.
    const match = step["output"];
    const matchIndex = match["index"];
    const matchCharacter = match["character"];
    const strategy = step["strategy"];


    // For dictionary updates, we can stop after the explanation.
    if (stepNumberSub == 2) {
        const dictionaryUpdate = step["dictionaryUpdate"];
        var explanation = "We are not performing a dictionary update as the string is already part of the dictionary.";
        if (dictionaryUpdate) {
            explanation = "We are adding the new entry <code>'" + dictionaryUpdate["entry"] + "'</code> at index <var>i</var> = " + dictionaryUpdate["index"];
            if (matchIndex === 0) {
                explanation += ". As the referenced index is <code>0</code>, this is the same as the second couple element <code>'" + matchCharacter.replace(" ", "&blank;") + "'</code>.";
            } else {
                explanation += ". The entry is the concatenation of the entry at the referenced (match) index " + matchIndex + " and the second couple element <code>'" + matchCharacter.replace(" ", "&blank;") + "'</code>.";
            }
        }
        explanationValue.html(explanation);
        return;
    }

    // Some encoding happened. Map the texts to the corresponding strategy tags.
    // We have to consider the match index as well as we do not use special tags for
    // no matches.
    var explanation = null;
    switch (strategy) {
        case "bestMatch":
            if (matchIndex === 0) {
                explanation = "We do not have a match inside the dictionary and therefore";
            } else {
                explanation = "We choose the longest match at dictionary index <var>i</var> = " + matchIndex + " and therefore";
            }
            break;
        case "lastPart":
            if (matchIndex === 0) {
                explanation = "We cannot choose the best match. As there is only one character remaining in the input, we therefore";
            } else {
                explanation = "We choose the second best match at dictionary index <var>i</var> = " + matchIndex + ". We cannot choose the best match as the second couple element would be empty otherwise. We therefore";
            }
            break;
        default:
            handleApiError("Unknown strategy for choosing the best match.");
            break;
    }

    // Create the output couple.
    const couple = "(" + matchIndex + ", C('" + matchCharacter + "'))"

    // Add the output couple to the explanation text and display the text.
    explanation += " output <code>" + couple + "</code>."
    explanationValue.html(explanation);

    // Only add the couple to the output if we are moving forward as in the other case
    // we always have to remove a couple instead.
    if (isForward) {
        currentOutput.push(couple);
        outputArea.text(currentOutput.join(" "));
    }
}
