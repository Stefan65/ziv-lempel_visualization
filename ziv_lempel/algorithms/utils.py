#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Utility methods used by multiple implementations.
"""


def find_best_match_in_dictionary_list(dictionary, text):
    """
    Find the best match for the beginning of the given text inside the dictionary.

    .. note::
       This implementation assumes that there are no duplicate entries inside the
       dictionary, as it is possible for the implicit dictionary used by the LZ77
       algorithm.

    :param dictionary: The dictionary to use.
    :type dictionary: list

    :param text: The text to use for matching.
    :type text: str

    :return: The match index (zero-based) and the match length.
    :rtype: tuple[int, int]
    """
    # Initialize the needed values.
    best_length = 0
    best_index = -1

    for index, value in enumerate(dictionary):
        length = len(value)
        text_first_length_characters = text[:length]

        # Continue with the next dictionary entry if there is no match.
        if value != text_first_length_characters:
            continue

        # Update the values for the best match.
        if length > best_length:
            best_length = length
            best_index = index

    return best_index, best_length


def convert_dictionary_list_to_real_dictionary(dictionary):
    """
    Convert the given dictionary list into a real dictionary.

    :param dictionary: The dictionary list (zero-based).
    :type dictionary: list[str]

    :return: A real dictionary created from the dictionary list (one-based).
    :rtype: dict[int, str]
    """
    real = dict(zip(range(1, len(dictionary) + 1), dictionary))
    return real


def mask_character(character):
    """
    Mask the given character if it is an apostrophe.

    :param character: The character to mask.
    :type text: str

    :return: The character, maybe masked.
    :rtype: str
    """
    if character == "'":
        return "''"
    return character
