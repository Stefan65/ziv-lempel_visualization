#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Perform data compression using the LZ78 algorithm as detailed in [Say17] while keeping
track of all the steps needed for it.
"""

import ziv_lempel.algorithms.utils as utils


class Lz78Compressor:
    """
    Perform LZ78 compression.
    """

    _dictionary = None
    """
    The dictionary as a list.

    :type: :class:`list` [:class:`str`]
    """

    def __init__(self):
        self._dictionary = []

    def compress(self, input_text):
        """
        Compress the given text.

        :param input_text: The input text to compress.
        :type input_text: str

        :return: The list of steps with details regarding the compression workflow.
        :rtype: list[dict[str, object]]
        """
        # The output variable for holding the single steps.
        output = []

        # Initialize the remaining text.
        text = input_text

        # Initialize the dictionary.
        self._dictionary = []

        # Handle the input.
        while True:
            # Prepare an empty output dictionary.
            step = {}

            # Exit the loop if the remaining text is empty.
            if not text:
                break

            # Determine the best match.
            match_index, match_length = utils.find_best_match_in_dictionary_list(
                self._dictionary, text
            )

            # Determine the following character.
            try:
                following_character = text[match_length]
            except IndexError:
                following_character = None

            # Determine the number of characters being encoded/compressed.
            encoded_character_count = match_length + 1

            # Determine the new dictionary entry and add it.
            new_dictionary_entry = text[:encoded_character_count]
            if new_dictionary_entry not in self._dictionary:
                self._dictionary.append(new_dictionary_entry)
                index = len(self._dictionary)
                step["dictionaryUpdate"] = {
                    "index": index,
                    "entry": new_dictionary_entry,
                }
            else:
                step["dictionaryUpdate"] = None

            # Update the remaining input text.
            old_text = text
            text = text[encoded_character_count:]

            # If the text is empty after this step, we use the second best match.
            # We have to use this approach as the second element of the couple cannot
            # be empty.
            if not text:
                # This is the last part/couple to be transmitted.
                step["strategy"] = "lastPart"

                # Retrieve the text for the last couple, with the last character being
                # removed. This is then used to find the exact match - the second best
                # match which has to be available due to how LZ78 creates its
                # dictionary.
                to_lookup = old_text[:-1]
                match_index = 0
                if not to_lookup:
                    # We have no match if the last couple just encodes a single
                    # character.
                    match_index = 0
                else:
                    # Search for the match. This has to be available, as the already
                    # updated text we use in `if not text` would not be empty
                    # otherwise and due to the way LZ78 creates its dictionary.
                    # We have to add one as our internal dictionary is zero-based, but
                    # the output dictionary is one-based (index zero is some sort of
                    # placeholder for "the character inside this couple is not part of
                    # the dictionary at the moment").
                    match_index = self._dictionary.index(to_lookup) + 1

                # Create the output dictionary.
                # The character is the last character from the input text (the last
                # character of `old_text`).
                step["output"] = {
                    "index": match_index,
                    "character": utils.mask_character(old_text[-1]),
                }
            else:
                # There will be more couples afterwards.
                step["output"] = {
                    "index": match_index + 1,
                    "character": utils.mask_character(following_character),
                }
                step["strategy"] = "bestMatch"

            # Add the step to the output.
            output.append(step)

        # Output the result.
        return output


class Lz78Decompressor:
    """
    Perform LZ78 decompression.
    """

    _dictionary = None
    """
    The dictionary as a list.

    :type: :class:`list` [:class:`str`]
    """

    def __init__(self):
        self._dictionary = []

    def decompress(self, compressed_data):
        """
        Decompress the given text.

        :param compressed_data: The compressed data to decompress.
        :type compressed_data: str

        :return: The list of steps with details regarding the decompression workflow.
        :rtype: list[dict[str, object]]

        :raises ValueError: The input is malformed.
        """
        # The output variable for holding the single steps.
        output = []

        # Initialize the dictionary.
        self._dictionary = []

        # Handle empty inputs.
        if not compressed_data:
            return output

        # Split the input into couples.
        couples = Lz78Decompressor._split_into_couples(compressed_data)
        if not couples:
            raise ValueError("Invalid couples.")

        # Handle all the couples.
        for couple in couples:
            # Prepare an empty output dictionary.
            step = {}

            # Extract the single couple values. The called method performs the required
            # data validation as well.
            index, character = Lz78Decompressor._extract_couple_elements(couple)

            if index == 0:
                # There is no match inside the dictionary, so just output the single
                # character.
                if character not in self._dictionary:
                    self._dictionary.append(character)
                    step["dictionaryUpdate"] = {
                        "index": len(self._dictionary),
                        "entry": character,
                    }
                else:
                    step["dictionaryUpdate"] = None
                step["newCharacters"] = character
            else:
                # There is a match inside the dictionary.
                try:
                    text = self._dictionary[index - 1]
                except IndexError:
                    # Catch invalid indices being passed.
                    raise ValueError(
                        f'"{str(couple)}" references an invalid dictionary entry.'
                    )

                # Set the output data and update the dictionary.
                text = text + character
                if text not in self._dictionary:
                    self._dictionary.append(text)
                    step["dictionaryUpdate"] = {
                        "index": len(self._dictionary),
                        "entry": text,
                    }
                else:
                    step["dictionaryUpdate"] = None

                step["newCharacters"] = text

            # Add the couple data.
            step["couple"] = {"index": index, "character": character}

            # Add the step to the output.
            output.append(step)

        # Output the result.
        return output

    @staticmethod
    def _split_into_couples(text):
        """
        Split the given text into couples.

        :param text: The text to split.
        :type text: str

        :return: The single couples.
        :rtype: list[str]
        """
        # Remove the spaces between the couples to make the couple detection more
        # robust.
        text = text.strip().replace(") (", ")(")
        return text.split(")(")

    @staticmethod
    def _extract_couple_elements(couple):
        """
        Extract the two elements from the given couple.

        Additionally, basic verification tasks are performed.

        :param couple: The couple to handle.
        :type couple: str

        :return: The elements extracted from the couple, in the order index and
                 character.
        :rtype: tuple[int, str]

        :raises ValueError: The input validation failed.
        """
        # Get single elements.
        couple_elements = couple.split(",")

        # Special case where the character is a comma. We set the value again and
        # remove the last element created by the split on the character comma.
        if len(couple_elements) == 3 and "','" in couple:
            couple_elements[1] = "','"
            del couple_elements[2]

        # This is no couple.
        if len(couple_elements) != 2:
            raise ValueError(f'"{str(couple)}" is not a couple.')

        # Handle the first element - the index.
        # This typically has the format :code:`(4` or :code:`4`.
        index = couple_elements[0]
        index = index.replace("(", "").strip()
        try:
            index = int(index)
        except ValueError:
            # Not an integer.
            raise ValueError(f'"{str(couple)}" has an invalid index value.')
        if index < 0:
            # Out of bounds.
            raise ValueError(f'"{str(couple)}" has an index value out of bounds.')

        # Handle the second element - the character.
        # This typically has the format :code:`C('a')` or :code:`'a'`.
        character = couple_elements[1].strip()
        if "''''" in character:
            # Escaped apostrophe.
            character = "'"
        else:
            # No special escape sequence.
            character = character.split("'")
            if len(character) != 3:
                # Only single quotation marks are allowed.
                raise ValueError(f'"{str(couple)}" has an invalid character masking.')
            # The character itself is between the two quotation marks.
            character = character[1]
            if len(character) != 1:
                # Not a single character, but a string.
                raise ValueError(f'"{str(couple)}" has more than one character.')

        # Return the values if all checks passed.
        return index, character
