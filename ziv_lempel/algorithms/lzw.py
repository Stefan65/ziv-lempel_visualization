#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Perform data compression using the LZW algorithm as detailed in [Say17] while keeping
track of all the steps needed for it.
"""

import ziv_lempel.algorithms.utils as utils


class LzwCompressor:
    """
    Perform LZW compression.
    """

    _dictionary = None
    """
    The dictionary as a list.

    :type: :class:`list` [:class:`str`]
    """

    def __init__(self):
        self._dictionary = []

    def compress(self, input_text, initial_dictionary=None):
        """
        Compress the given text.

        :param input_text: The input text to compress.
        :type input_text: str

        :param initial_dictionary: The initial dictionary to use. This should either
                                   be a string of characters in the order in which they
                                   should appear inside the dictionary or :code:`None`.
                                   Use :code:`None` to create the dictionary
                                   automatically by using the characters from the input
                                   text, sorted alphabetically.
        :type initial_dictionary: str or None

        :return: The list of steps with details regarding the compression workflow
                 and the initial dictionary.
        :rtype: tuple[list[dict[str, object]], dict[str, str]]

        :raises ValueError: The initial dictionary is not valid.
        """
        # The output variable for holding the single steps.
        output = []

        # Initialize the remaining text.
        text = input_text

        # Initialize the dictionary.
        self._dictionary = LzwCompressor._prepare_dictionary(
            input_text, initial_dictionary
        )

        # Prepare the initial dictionary for the output.
        initial_dictionary_output = utils.convert_dictionary_list_to_real_dictionary(
            self._dictionary
        )

        # Handle the input.
        while True:
            # Prepare an empty output dictionary.
            step = {}

            # Exit the loop if the remaining text is empty.
            if not text:
                break

            # Determine the best match.
            match_index, match_length = utils.find_best_match_in_dictionary_list(
                self._dictionary, text
            )

            # Determine the new dictionary entry and add it to the dictionary.
            new_dictionary_entry = self._dictionary[match_index]
            try:
                new_dictionary_entry += text[match_length]
            except IndexError:
                pass
            if new_dictionary_entry not in self._dictionary:
                self._dictionary.append(new_dictionary_entry)
                index = len(self._dictionary)
                step["dictionaryUpdate"] = {
                    "index": index,
                    "entry": new_dictionary_entry,
                }
            else:
                step["dictionaryUpdate"] = None

            # Update the remaining input text.
            text = text[match_length:]

            step["output"] = match_index + 1

            # Add the step to the output.
            output.append(step)

        # Output the result.
        return output, initial_dictionary_output

    @staticmethod
    def _prepare_dictionary(text, initial):
        """
        Prepare the dictionary.

        If the initial dictionary is :code:`None`, we use all the characters inside
        the text, sorted in alphabetical order.

        If the initial dictionary is available, we use it if it contains all characters
        from the input text and does not contain any duplicates.

        :param text: The input text.
        :type text: str

        :param initial: The initial dictionary.
        :type initial: str or None

        :return: The dictionary to use.
        :rtype: list[str]

        :raises ValueError: There are duplicates inside the given dictionary or
                            characters are missing inside it.
        """
        # Get all unique characters from the input text.
        characters_in_text = set(text) if text else set()

        # Detect duplicates for a given initial dictionary.
        if initial and not len(initial) == len(set(initial)):
            raise ValueError("Duplicates inside initial dictionary.")

        # If there is no initial dictionary specified, return a sorted list of all
        # unique characters inside the input text.
        if not initial:
            characters_in_text = list(characters_in_text)
            characters_in_text.sort()
            return characters_in_text

        # If there is an initial dictionary, check whether all characters from the
        # input text are part of it and throw an error if not.
        for character in characters_in_text:
            if character not in initial:
                raise ValueError(
                    f"Text contains '{character}' which is not part of the initial "
                    + "dictionary."
                )

        # The given initial dictionary is valid and can be used.
        return list(initial)


class LzwDecompressor:
    """
    Perform LZW decompression.
    """

    _dictionary = None
    """
    The dictionary as a list.

    :type: :class:`list` [:class:`str`]
    """

    def __init__(self):
        self._dictionary = []

    def decompress(self, compressed_data, initial_dictionary):
        """
        Decompress the given text.

        :param compressed_data: The compressed data to decompress.
        :type compressed_data: str

        :param initial_dictionary: The initial dictionary to use. This has to be given
                                   by passing a string with the characters in the order
                                   as they should appear inside the dictionary.
        :type initial_dictionary: str

        :return: The list of steps with details regarding the decompression workflow
                 and the initial dictionary.
        :rtype: tuple[list[dict[str, object]], dict[str, str]]

        :raises ValueError: The input is malformed.
        """
        # The output variable for holding the single steps.
        output = []

        # Handle empty inputs.
        if not compressed_data:
            return output

        # Initialize the dictionary.
        self._dictionary = LzwDecompressor._prepare_dictionary(initial_dictionary)

        # Prepare the initial dictionary for the output.
        initial_dictionary_output = utils.convert_dictionary_list_to_real_dictionary(
            self._dictionary
        )

        # Split the input into indices.
        indices = LzwDecompressor._split_into_indices(compressed_data)
        if not indices:
            raise ValueError("Invalid indices.")

        # Use an own variable as determining the length every time may increase the
        # costs.
        dictionary_length = len(self._dictionary)

        # The string saved from the previous step.
        saved_string = None

        # Handle all the indices.
        for index in indices:
            # Prepare an empty output dictionary and add the parsed index.
            step = {"index": index}

            # Save the value as we use it multiple times.
            dictionary_length1 = dictionary_length + 1

            # Abort if the index is too large.
            if index > dictionary_length1:
                raise ValueError(
                    f"'{str(index)}' references an invalid dictionary entry."
                )

            # Catch the special case where we want to reference a dictionary entry
            # before assignment.
            if index == dictionary_length1:
                # Reference before assignment, so work with the saved string.
                new_characters = saved_string + saved_string[0]
                strategy = "referenceBeforeAssignment"
            else:
                # Reference after assignment, so use the dictionary entry itself.
                new_characters = self._dictionary[index - 1]
                strategy = "normalReference"

            # Add the new characters and the strategy to the output.
            step["newCharacters"] = new_characters
            step["strategy"] = strategy

            # Check if a string has been saved before.
            if not saved_string:
                # There is no saved string at the beginning. For this reason we are not
                # doing a dictionary update here.
                saved_string = new_characters
                step["savedString"] = saved_string
                step["dictionaryUpdate"] = None
            else:
                # There is a saved string. Add the first new character and add the
                # complete dictionary entry (we do not use incomplete dictionary
                # entries).
                new_entry = saved_string + new_characters[0]
                step["dictionaryUpdate"] = {
                    "index": dictionary_length1,
                    "part1": saved_string,
                    "part2": new_characters[0],
                }
                self._dictionary.append(new_entry)
                dictionary_length = dictionary_length1

                # Update the saved string which is the start of the next dictionary
                # entry.
                saved_string = self._dictionary[index - 1]
                step["savedString"] = saved_string

            # Add the step to the output.
            output.append(step)

        # Output the result.
        return output, initial_dictionary_output

    @staticmethod
    def _prepare_dictionary(initial):
        """
        Prepare the dictionary.

        :param initial: The initial dictionary.
        :type initial: str or None

        :return: The dictionary to use.
        :rtype: list[str]

        :raises ValueError: There are duplicates inside the given dictionary.
        """
        # Detect duplicates for the given initial dictionary.
        if not len(initial) == len(set(initial)):
            raise ValueError("Duplicates inside initial dictionary.")

        # The given initial dictionary is valid and can be used.
        return list(initial)

    @staticmethod
    def _split_into_indices(text):
        """
        Extract the indices from the given string.

        Additionally, basic verification tasks are performed.

        :param text: The text to split.
        :type text: str

        :return: The indices extracted from the text.
        :rtype: list[int]

        :raises ValueError: The input validation failed.
        """
        # Split on whitespace.
        text_split = text.strip().split(" ")

        # Prepare the output list.
        indices = []

        # Try to convert each of the indices to an integer value and throw an error
        # if this does not work in at least one case.
        for index in text_split:
            try:
                indices.append(int(index))
            except ValueError:
                raise ValueError(f"'{index}' is not a valid index.")

        # Make sure that the given indices are one-based and not negative.
        for index in indices:
            if index < 1:
                raise ValueError(
                    f"'{str(index)}' is smaller than 1 and therefore an invalid index."
                )

        # Return the indices if they are valid.
        return indices
