#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tuple lexer and parsers.

This module is currently unused, but could be used as a replacement for the tuple
parsers inside the decompression classes. In this case you still would have to add
the corresponding range checks as they are omitted from the classes of this module.

The parsers in this module should be considered experimental as they have not been
thoroughly tested. In fact they should be more stable than the parser code currently
used as it does not rely on string splitting approaches, but makes use of a real lexer
and parser based upon a grammar (using regular expressions). The existing parser is
less strict in some cases.

There is some disadvantage, too: While the current parsers provide rather good/exact
error messages if the input is malformed (on tuple level), the classes from this
module will just print the position inside the input string which makes it harder to
spot the error in some cases.

Example usage:

.. code-block:: python

   triples = "(0, 0, 'A') (1, 1, C('B')) (2, 2, '''')"
   triples_parsed = lex_and_parse(TripleParser, triples)
   assert(triples_parsed == [(0, 0, "A"), (1, 1, "B"), (2, 2, "'")])

   couples = "(0, 'A') (1, C('B')) (2, '''')"
   couples_parsed = lex_and_parse(CoupleParser, couples)
   assert(couples_parsed == [(0, "A"), (1, "B"), (2, "'")])
"""

from rply import ParserGenerator, LexerGenerator, LexingError, ParsingError


class TupleLexer:
    """
    Lexer configuration for tuples.
    """

    tokens = [
        "LPAREN",
        "RPAREN",
        "COMMA",
        "NUMBER",
        "CHARACTER",
        "CHARACTER_APOSTROPHE",
        "CODE_FUNCTION",
    ]
    """
    List of tokens known by the lexer.

    :type: :class:`list` [:class:`str`]
    """

    @staticmethod
    def get_lexer():
        """
        Create a new lexer.

        :return: A new lexer instance.
        :rtype: rply.lexer.Lexer
        """
        generator = TupleLexer._generate_lexer()
        return generator.build()

    @staticmethod
    def _generate_lexer():
        """
        Generate the lexer itself.

        :return: The generated lexer.
        :rtype: rply.lexergenerator.LexerGenerator
        """
        generator = LexerGenerator()

        # Add the rules.
        # `CHARACTER_APOSTROPHE` has precedence over a regular `CHARACTER`.
        generator.add("LPAREN", r"\(")
        generator.add("RPAREN", r"\)")
        generator.add("COMMA", r",")
        generator.add("NUMBER", r"\d+")
        generator.add("CHARACTER_APOSTROPHE", r"''''")
        generator.add("CHARACTER", r"'.'")
        generator.add("CODE_FUNCTION", r"C")

        # Ignore whitespace
        generator.ignore(r"\s+")

        return generator


class TripleParser:
    """
    Parser configuration for triples as used by LZ77.
    """

    @staticmethod
    def get_parser():
        """
        Create a new parser.

        :return: A new parser instance.
        :rtype: rply.parser.Parser
        """
        generator = TripleParser._generate_parser()
        return generator.build()

    @staticmethod
    def _generate_parser():
        """
        Generate the parser itself.

        :return: The generated parser.
        :rtype: rply.parsergenerator.ParserGenerator
        """
        generator = ParserGenerator(TupleLexer.tokens)

        # The start production for a list of triples.
        @generator.production("triples : LPAREN triple RPAREN triples")
        @generator.production("triples : LPAREN triple RPAREN")
        def triples(tokens):
            try:
                data = tokens[3]
            except IndexError:
                data = []
            data.insert(0, tokens[1])
            return data

        # The production for one triple.
        @generator.production("triple : offset COMMA length COMMA character")
        @generator.production("triple : offset COMMA length COMMA character_apostrophe")
        @generator.production("triple : offset COMMA length COMMA character_code")
        def triple(tokens):
            return tokens[0], tokens[2], tokens[4]

        # The production for the offset.
        @generator.production("offset : NUMBER")
        def offset(tokens):
            return int(tokens[0].getstr())

        # The production for the length.
        @generator.production("length : NUMBER")
        def length(tokens):
            return int(tokens[0].getstr())

        # The production for the character.
        @generator.production("character : CHARACTER")
        def character(tokens):
            char = tokens[0].getstr()[1:-1]
            if char == "'":
                position = tokens[0].getsourcepos()
                raise ValueError(
                    f"The apostrophe at index {position.idx} has to be "
                    + "escaped/masked with another one."
                )
            return char

        # The production for the masked apostrophe character.
        @generator.production("character_apostrophe : CHARACTER_APOSTROPHE")
        def character_apostrophe(_):
            return "'"

        # The production for a character enclosed into a code function `C()`.
        @generator.production("character_code : CODE_FUNCTION LPAREN character RPAREN")
        @generator.production(
            "character_code : CODE_FUNCTION LPAREN character_apostrophe RPAREN"
        )
        def character_code(tokens):
            return tokens[2]

        # Report errors.
        @generator.error
        def error_handle(token):
            if token.gettokentype() == "$end":
                raise ValueError("Reached end of input while still parsing it.")
            raise ValueError(
                f"Syntax error detected for token {token.gettokentype()} "
                + f"'{token.getstr()}' at position {token.getsourcepos().idx}."
            )

        return generator


class CoupleParser:
    """
    Parser configuration for couples as used by LZ78.
    """

    @staticmethod
    def get_parser():
        """
        Create a new parser.

        :return: A new parser instance.
        :rtype: rply.parser.Parser
        """
        generator = CoupleParser._generate_parser()
        return generator.build()

    @staticmethod
    def _generate_parser():
        """
        Generate the parser itself.

        :return: The generated parser.
        :rtype: rply.parsergenerator.ParserGenerator
        """
        generator = ParserGenerator(TupleLexer.tokens)

        # The start production for a list of couples.
        @generator.production("couples : LPAREN couple RPAREN couples")
        @generator.production("couples : LPAREN couple RPAREN")
        def couples(tokens):
            try:
                data = tokens[3]
            except IndexError:
                data = []
            data.insert(0, tokens[1])
            return data

        # The production for one couple.
        @generator.production("couple : index COMMA character")
        @generator.production("couple : index COMMA character_apostrophe")
        @generator.production("couple : index COMMA character_code")
        def couple(tokens):
            return tokens[0], tokens[2]

        # The production for the index.
        @generator.production("index : NUMBER")
        def index(tokens):
            return int(tokens[0].getstr())

        # The production for the character.
        @generator.production("character : CHARACTER")
        def character(tokens):
            char = tokens[0].getstr()[1:-1]
            if char == "'":
                position = tokens[0].getsourcepos()
                raise ValueError(
                    f"The apostrophe at index {position.idx} has to be "
                    + "escaped/masked with another one."
                )
            return char

        # The production for the masked apostrophe character.
        @generator.production("character_apostrophe : CHARACTER_APOSTROPHE")
        def character_apostrophe(_):
            return "'"

        # The production for a character enclosed into a code function `C()`.
        @generator.production("character_code : CODE_FUNCTION LPAREN character RPAREN")
        @generator.production(
            "character_code : CODE_FUNCTION LPAREN character_apostrophe RPAREN"
        )
        def character_code(tokens):
            return tokens[2]

        # Report errors.
        @generator.error
        def error_handle(token):
            if token.gettokentype() == "$end":
                raise ValueError("Reached end of input while still parsing it.")
            raise ValueError(
                f"Syntax error detected for token {token.gettokentype()} "
                + f"'{token.getstr()}' at position {token.getsourcepos().idx}."
            )

        return generator


def lex_and_parse(parser_class, string):
    """
    Parse the given string with the given parser. This will use the tuple lexer
    beforehand.

    :param parser_class: The parser class to use.
    :type parser_class: type

    :param string: The string to parse.
    :type string: str

    :return: The parsed string.
    :rtype: list[tuple[int, int, str]] or list[tuple[int, str]]

    :raises ValueError: The given input is malformed and either raises a lexing,
                        parsing or value error.
    """
    # Run the lexer.
    lexer_result = TupleLexer.get_lexer().lex(string)

    # Run the parser.
    try:
        parser_result = parser_class.get_parser().parse(lexer_result)
    except LexingError as error:
        position = error.getsourcepos()
        index = position.idx
        raise ValueError(f'Lexing error at position {index} for "{string[index]}".')
    except ParsingError as error:
        position = error.getsourcepos()
        index = position.idx
        raise ValueError(f'Parsing error at position {index} for "{string[index]}".')
    return parser_result
