#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Perform data compression using the LZ77 algorithm as detailed in [Say17] while keeping
track of all the steps needed for it.
"""

from ziv_lempel.api.configuration import Lz77Configuration as config
import ziv_lempel.algorithms.utils as utils


class Lz77Compressor:
    """
    Perform LZ77 compression.
    """

    _sb_size = 0
    """
    The size of the search buffer.

    :type: :class:`int`
    """

    _lab_size = 0
    """
    The size of the look-ahead buffer.

    :type: :class:`int`
    """

    def __init__(self, sb_size, lab_size):
        """
        :param sb_size: The search buffer size to use.
        :type sb_size: int

        :param lab_size: The look-ahead buffer size to use.
        :type lab_size: int

        :raises ValueError: At least one of the buffers would be empty.
        """
        if sb_size >= 1:
            self._sb_size = sb_size
        else:
            raise ValueError("The search buffer cannot be empty.")

        if lab_size >= 1:
            self._lab_size = lab_size
        else:
            raise ValueError("The look-ahead buffer cannot be empty.")

    def compress(self, input_text, initial_sb=None):
        """
        Compress the given text using the specified buffer sizes and the initial
        search buffer.

        :param input_text: The input text to compress.
        :type input_text: str

        :param initial_sb: The initial content of the search buffer. Empty by default.
        :type initial_sb: str or None

        :return: The list of steps with details regarding the compression workflow.
        :rtype: list[dict[str, object]]
        """
        # The output variable for holding the single steps.
        output = []

        # Initialize the search buffer.
        sb = self._get_search_buffer_part(initial_sb) if initial_sb else ""

        # Initialize the offset inside the look-ahead buffer.
        lab_offset = 0

        # Handle the input.
        while True:
            # Prepare an empty output dictionary.
            step = {}

            # Determine the current look-ahead buffer content.
            lab = self._get_look_ahead_buffer_part(input_text, lab_offset)

            # Exit the loop if the look-ahead buffer is empty.
            if not lab:
                break

            # Set some of the already known output values.
            step["searchBuffer"] = sb
            step["lookAheadBuffer"] = lab
            step["matches"] = []

            # Determine the indices of the matches inside the search buffer.
            match_indices = self._find_all(sb, lab[0])

            # Handle the different cases for a match.
            if len(lab) == 1 or not match_indices:
                # The look-ahead buffer has only one remaining character which needs
                # to be used as the third triple element or no match could be found.

                # Set the output values.
                step["output"] = {
                    "offset": 0,
                    "length": 0,
                    "character": utils.mask_character(lab[0]),
                }

                # Determine the used strategy.
                if len(lab) == 1:
                    step["strategy"] = "lastCharacter"
                else:
                    step["strategy"] = "noMatch"

                # We only transmit one character.
                step_size = 1
            else:
                # There is more than one character inside the look-ahead buffer and at
                # least one match has been found.

                # Count the matches.
                match_count = len(match_indices)

                # Determine the offset and length values.
                match_offsets = Lz77Compressor._convert_indices_to_offsets(
                    sb, match_indices
                )
                match_lengths = Lz77Compressor._find_match_lengths(
                    sb, lab, match_indices
                )

                # Determine the best match.
                best_match_array_index, best_match_cause = self._find_best_match(
                    match_lengths
                )
                best_offset = match_offsets[best_match_array_index]
                best_length = match_lengths[best_match_array_index]
                try:
                    output_character = utils.mask_character(lab[best_length])
                except IndexError:
                    # The best match will go until the end of the look-ahead buffer,
                    # so there is no real following character.

                    # We can reduce the length by one here without having to fear that
                    # it will be zero afterwards. If there is only one character left
                    # inside the look-ahead buffer, we would have chosen the other case
                    # of the if statement above already.
                    best_length -= 1
                    step["shortenedLastMatch"] = True
                    output_character = utils.mask_character(lab[-1])

                # Transmit all matches.
                for index in range(match_count):
                    match = {
                        "offset": match_offsets[index],
                        "length": match_lengths[index],
                    }
                    step["matches"].append(match)

                # Transmit the best match.
                step["strategy"] = best_match_cause
                step["output"] = {
                    "offset": best_offset,
                    "length": best_length,
                    "character": output_character,
                }

                # We transmit "length of the best match + 1" characters.
                step_size = best_length + 1

            # Add the step to the output.
            output.append(step)

            # Update the offset and the search buffer.
            # For the search buffer, we have to consider the emitted characters
            # from the look-ahead buffer as well (sliding window principle).
            lab_offset += step_size
            sb = self._get_search_buffer_part(sb + lab[:step_size])

        # Output the result.
        return output

    def _get_search_buffer_part(self, sb):
        """
        Get the last "search buffer size" characters from the search buffer.

        This removes the superfluous characters on the left.

        :param sb: The current search buffer to get the characters from.
        :type sb: str

        :return: The requested part of the search buffer with not more than "search
                 buffer size" characters.
        :rtype: str
        """
        return sb[-self._sb_size :]

    def _get_look_ahead_buffer_part(self, lb, offset=0):
        """
        Get "look-ahead buffer size" characters from the look-ahead buffer, starting
        at the offset.

        :param lb: The input text to get the characters from.
        :type lb: str

        :param offset: The offset to use.
        :type offset: int

        :return: The requested part of the look-ahead buffer with not more than
                 "look-ahead buffer size" characters.
        :rtype: str
        """
        return lb[offset : offset + self._lab_size]

    @staticmethod
    def _convert_indices_to_offsets(sb, indices):
        """
        Convert the search buffer string indices to offset values.

        :param sb: The search buffer.
        :type sb: str

        :param indices: The list of indices.
        :type indices: list[int]

        :return: The corresponding offsets.
        :rtype: list[int]
        """
        offsets = []
        sb_length = len(sb)
        for index in indices:
            offsets.append(sb_length - index)
        return offsets

    @staticmethod
    def _find_match_lengths(sb, lab, indices):
        """
        Determine the length of each of the matches.

        :param sb: The search buffer to use.
        :type sb: str

        :param lab: The look-ahead buffer to use.
        :type lab: str

        :param indices: The list of indices where the matches start inside the search
                        buffer.
        :type indices: list[int]

        :return: The array with the determine lengths.
        :rtype: list[int]
        """
        lengths = []

        # Allow finding matches across the buffer borders by using a combined buffer.
        # Matches always start inside the look-ahead buffer. Their start positions have
        # been determined before while only looking at the search buffer.
        sb_lab = sb + lab
        sb_lab_length = len(sb_lab)
        lab_length = len(lab)

        for index in indices:
            # For determining the length we use a rather simple approach here.
            length = 0
            position = index

            while sb_lab[position] == lab[length]:
                length += 1
                position += 1
                # Avoid index errors.
                if position >= sb_lab_length or length >= lab_length:
                    break

            # The maximum value is the length of the look-ahead buffer.
            # Please note that we do not use (length - 1) here to improve handling
            # for the "shortenedLastMatch" strategy.
            length = min(length, lab_length)

            # Add the length value.
            lengths.append(length)

        return lengths

    @staticmethod
    def _find_best_match(lengths):
        """
        Find the best match from the given lengths.

        :param lengths: The length values to determine the best match from.
        :type lengths: list[int]

        :return: The array index and explanation for the best match.
        :rtype: tuple[int, str]
        """
        # Initialize the needed values.
        best_length = -1
        best_length_index = None
        cause = None

        # Handle the special case with only one match.
        if len(lengths) == 1:
            return 0, "onlyMatch"

        for index, length in enumerate(lengths):
            # All of our entries are ordered by decreasing offset.
            # By checking the similarity of lengths, we update equal lengths to the
            # smaller offset.
            if length > best_length:
                best_length = length
                best_length_index = index
                cause = "longestMatch"
            elif length == best_length:
                best_length_index = index
                cause = "smallestOffset"

        return best_length_index, cause

    @staticmethod
    def _find_all(text, key_character):
        """
        Find all occurrences of the given character inside the text.

        :param text: The text to search inside.
        :type text: str

        :param key_character: The character to use as search key.
        :type key_character: str

        :return: A list of match positions.
        :rtype: list[int]
        """
        matches = []
        for index, character in enumerate(text):
            if character == key_character:
                matches.append(index)
        return matches


class Lz77Decompressor:
    """
    Perform LZ77 decompression.
    """

    _sb_size = 0
    """
    The size of the search buffer.

    :type: :class:`int`
    """

    _lab_size = 0
    """
    The size of the look-ahead buffer.

    :type: :class:`int`
    """

    def __init__(self, sb_size, lab_size):
        """
        :param sb_size: The search buffer size to use.
        :type sb_size: int

        :param lab_size: The look-ahead buffer size to use.
        :type lab_size: int

        :raises ValueError: At least one of the buffers would be empty.
        """
        if sb_size >= 1:
            self._sb_size = sb_size
        else:
            raise ValueError("The search buffer cannot be empty.")

        if lab_size >= 1:
            self._lab_size = lab_size
        else:
            raise ValueError("The look-ahead buffer cannot be empty.")

    def decompress(self, compressed_data, initial_rd=None):
        """
        Decompress the given text using the specified buffer sizes and the initial
        search buffer.

        :param compressed_data: The compressed data to decompress.
        :type compressed_data: str

        :param initial_rd: The initial content of "recently decompressed". Empty by
                           default.
        :type initial_rd: str or None

        :return: The list of steps with details regarding the decompression workflow.
        :rtype: list[dict[str, object]]

        :raises ValueError: The input is malformed.
        """
        # The output variable for holding the single steps.
        output = []

        # Initialize the recently decompressed data.
        rd = self._get_recently_decompressed_part(initial_rd) if initial_rd else ""

        # Handle empty inputs.
        if not compressed_data:
            return output

        # Split the input into triples.
        triples = Lz77Decompressor._split_into_triples(compressed_data)
        if not triples:
            raise ValueError("Invalid triples.")

        # Handle all the triples.
        for triple in triples:
            # Prepare an empty output dictionary.
            step = {}

            # Extract the single triple values. The called method performs the required
            # data validation as well.
            offset, length, character = self._extract_triple_elements(triple)

            # The recently decompressed data.
            step["recentlyDecompressed"] = rd

            # Handle the different cases for decompressing a triple.
            if offset == 0:
                # If the offset is zero, there has not been a match and we can just
                # add the new character to the output.
                step["overflow"] = None
                step["strategy"] = "appendOnly"
                step["newCharacters"] = character
            elif offset >= length:
                # If the offset is larger than the length, we do not have an overflow.
                # We can just copy the requested number of characters from the recently
                # decompressed data, starting at the offset.
                step["overflow"] = None
                step["strategy"] = "simpleCopy"
                # We use some string slicing here were we get all the characters from
                # the recently decompressed data starting at the offset. Afterwards we
                # retrieve just the first "length" characters and append the character
                # from the triple.
                last_characters = rd[-offset:]
                step["newCharacters"] = last_characters[:length] + character
            else:
                # If the length is greater than the offset, there is an overflow and we
                # have to handle this harder case.
                step["strategy"] = "continuePattern"

                # Determine the length of the overflow.
                overflow_length = length - offset

                # Get the last characters from the recently decompressed data. This
                # data will be repeated in the next step.
                last_characters = rd[-offset:]

                # Determine the maximum number of repetitions needed to fill the
                # complete look-ahead buffer, write the required number of repetitions
                # and only keep the characters which are required for the overflow.
                # We use an integer division here.
                repetitions = self._lab_size // offset + 1
                overflow = repetitions * last_characters
                overflow = overflow[:overflow_length]

                # Finally, we can write the output.
                step["overflow"] = overflow
                step["newCharacters"] = last_characters + overflow + character

            # Add the triple data.
            step["triple"] = {
                "offset": offset,
                "length": length,
                "character": character,
            }

            # Add the step to the output.
            output.append(step)

            # Update the recently decompressed data (search buffer).
            rd = self._get_recently_decompressed_part(rd + step["newCharacters"])

        # Output the result.
        return output

    def _get_recently_decompressed_part(self, rd):
        """
        Get the last "search buffer size" characters from the recently decompressed
        data.

        This removes the superfluous characters on the left.

        :param rd: The current output to get the characters from.
        :type rd: str

        :return: The requested part of the of the data with not more than "search
                 buffer size" characters.
        :rtype: str
        """
        return rd[-self._sb_size :]

    @staticmethod
    def _split_into_triples(text):
        """
        Split the given text into triples.

        :param text: The text to split.
        :type text: str

        :return: The single triples.
        :rtype: list[str]
        """
        # Remove the spaces between the triples to make the triple detection more
        # robust.
        text = text.strip().replace(") (", ")(")
        return text.split(")(")

    def _extract_triple_elements(self, triple):
        """
        Extract the three elements from the given triple.

        Additionally, basic verification tasks are performed.

        :param triple: The triple to handle.
        :type triple: str

        :return: The elements extracted from the triple, in the order offset, length
                 and character.
        :rtype: tuple[int, int, str]

        :raises ValueError: The input validation failed.
        """
        # Get single elements.
        triple_elements = triple.split(",")

        # Special case where the character is a comma. We set the value again and
        # remove the last element created by the split on the character comma.
        if len(triple_elements) == 4 and "','" in triple:
            triple_elements[2] = "','"
            del triple_elements[3]

        # This is no triple.
        if len(triple_elements) != 3:
            raise ValueError('"' + str(triple) + '" is not a triple.')

        # Handle the first element - the offset.
        # This typically has the format :code:`(4` or :code:`4`.
        offset = triple_elements[0]
        offset = offset.replace("(", "").strip()
        try:
            offset = int(offset)
        except ValueError:
            # Not an integer.
            raise ValueError(f'"{str(triple)}" has an invalid offset value.')
        if offset < 0 or offset > config.SEARCH_BUFFER_SIZE_MAX:
            # Out of bounds (corresponds to maximal search buffer size).
            raise ValueError(f'"{str(triple)}" has an offset value out of bounds.')
        if offset > self._sb_size:
            # Out of bounds for the configured search buffer size.
            raise ValueError(
                f'"{str(triple)}" has an offset which does not match the search '
                + "buffer size."
            )

        # Handle the second element - the length.
        # This typically has the format :code:`4`.
        length = triple_elements[1].strip()
        try:
            length = int(length)
        except ValueError:
            # Not an integer.
            raise ValueError(f'"{str(triple)}" has an invalid length value.')
        if length < 0 or length > self._lab_size:
            # We would exceed the size of the sliding window.
            raise ValueError(f'"{str(triple)}" has a length value out of bounds.')
        if (offset == 0 and length != 0) or (length == 0 and offset != 0):
            # Invalid combinations for non-matches.
            raise ValueError(
                f'"{str(triple)}" has an invalid combination of length and offset '
                + "values."
            )

        # Handle the third element - the character.
        # This typically has the format :code:`C('a')` or :code:`'a'`.
        character = triple_elements[2].strip()
        if "''''" in character:
            # Escaped apostrophe.
            character = "'"
        else:
            # No special escape sequence.
            character = character.split("'")
            if len(character) != 3:
                # Only single quotation marks are allowed.
                raise ValueError(f'"{str(triple)}" has an invalid character masking.')
            # The character itself is between the two quotation marks.
            character = character[1]
            if len(character) != 1:
                # Not a single character, but a string.
                raise ValueError(f'"{str(triple)}" has more than one character.')

        # Return the values if all checks passed.
        return offset, length, character
