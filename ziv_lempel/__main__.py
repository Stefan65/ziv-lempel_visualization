#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The main entry point of the application starting the server.
"""

from flask import Flask, request, redirect
from flask_json import FlaskJSON, as_json

from ziv_lempel.api.lz77_api import Lz77Api
from ziv_lempel.api.lz78_api import Lz78Api
from ziv_lempel.api.lzw_api import LzwApi


# Create the flask application and initialize the JSON module with it.
APP = Flask("ziv_lempel")
FlaskJSON(APP)

# Do not mind whether trailing slashes have been passed or not and handle both cases
# in an equivalent manner.
APP.url_map.strict_slashes = False


@APP.route("/", methods=["GET"])
def landing_page():
    """
    Redirect to the LZ77 compression page when calling the index page.
    """
    # TODO: Maybe use a landing page later introducing the application including the
    #       source the implementation is based on [Say17].
    return redirect("/static/lz77/compression.html")


@APP.route("/api/lz77/compress", methods=["GET", "POST"])
@as_json
def lz77_compress():
    """
    The LZ77 compression endpoint handler.

    .. :quickref: LZ77; Perform LZ77 compression
    """
    if request.method == "GET":
        data = request.args.to_dict()
    else:
        data = request.get_json()

    if APP.debug:
        print(data)
    return Lz77Api.compress(data)


@APP.route("/api/lz77/decompress", methods=["GET", "POST"])
@as_json
def lz77_decompress():
    """
    The LZ77 decompression endpoint handler.

    .. :quickref: LZ77; Perform LZ77 decompression
    """
    if request.method == "GET":
        data = request.args.to_dict()
    else:
        data = request.get_json()

    if APP.debug:
        print(data)
    return Lz77Api.decompress(data)


@APP.route("/api/lz78/compress", methods=["GET", "POST"])
@as_json
def lz78_compress():
    """
    The LZ78 compression endpoint handler.

    .. :quickref: LZ78; Perform LZ78 compression
    """
    if request.method == "GET":
        data = request.args.to_dict()
    else:
        data = request.get_json()

    if APP.debug:
        print(data)
    return Lz78Api.compress(data)


@APP.route("/api/lz78/decompress", methods=["GET", "POST"])
@as_json
def lz78_decompress():
    """
    The LZ78 decompression endpoint handler.

    .. :quickref: LZ78; Perform LZ78 decompression
    """
    if request.method == "GET":
        data = request.args.to_dict()
    else:
        data = request.get_json()

    if APP.debug:
        print(data)
    return Lz78Api.decompress(data)


@APP.route("/api/lzw/compress", methods=["GET", "POST"])
@as_json
def lzw_compress():
    """
    The LZW compression endpoint handler.

    .. :quickref: LZW; Perform LZW compression
    """
    if request.method == "GET":
        data = request.args.to_dict()
    else:
        data = request.get_json()

    if APP.debug:
        print(data)
    return LzwApi.compress(data)


@APP.route("/api/lzw/decompress", methods=["GET", "POST"])
@as_json
def lzw_decompress():
    """
    The LZW decompression endpoint handler.

    .. :quickref: LZW; Perform LZW decompression
    """
    if request.method == "GET":
        data = request.args.to_dict()
    else:
        data = request.get_json()

    if APP.debug:
        print(data)
    return LzwApi.decompress(data)


# Avoid running it when creating the documentation.
if __name__ == "__main__":
    APP.run(debug=True)  # TODO: Disable debugging later.
