#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Dictionary based API for the LZ78 algorithm.
"""

from ziv_lempel.algorithms.lz78 import Lz78Decompressor, Lz78Compressor
from ziv_lempel.api.configuration import Lz78Configuration as config


class Lz78Api:
    """
    Methods for converting JSON-based API data for the LZ78 algorithm into a format
    which can be processed by Python.

    This class should be used in conjunction with the web-based API when the
    application is served by Flask, where all parameters are wrapped into a dictionary.
    :class:`~ziv_lempel.algorithms.lz78.Lz78Compressor` and
    :class:`~ziv_lempel.algorithms.lz78.Lz78Decompressor` use real method parameters for
    using this implementation inside another Python module - they return a regular
    Python dictionary with each of the steps instead of a dictionary where the steps
    are just a part of and do not incorporate length limitations for the input.
    """

    @staticmethod
    def compress(request_dict):
        """
        Perform LZ78 compression.

        :param request_dict: The data to use as input.
        :type request_dict: dict

        :return: The compression result or an error message.
        :rtype: dict
        """
        # Prepare the result dictionary.
        result = {}

        # Retrieve the input text.
        input_text = request_dict.get("inputText")
        if input_text and len(input_text) > config.INPUT_TEXT_LENGTH_MAX:
            result["errorMessage"] = "Input text too long."
            return result

        # There has not been an error.
        result["errorMessage"] = None

        # Perform the compression itself.
        compressor = Lz78Compressor()
        result["steps"] = compressor.compress(input_text)

        # Save the number of steps needed for compression.
        result["stepCount"] = len(result["steps"])

        # Return the resulting dictionary.
        return result

    @staticmethod
    def decompress(request_dict):
        """
        Perform LZ78 decompression.

        :param request_dict: The data to use as input.
        :type request_dict: dict

        :return: The decompression result or an error message.
        :rtype: dict
        """
        # Prepare the result dictionary.
        result = {}

        # Retrieve the compressed data couples.
        compressed_data = request_dict.get("compressedData")
        if compressed_data and len(compressed_data) > config.COMPRESSED_DATA_LENGTH_MAX:
            result["errorMessage"] = "Compressed data (couples) too long."
            return result

        # There has not been an error.
        result["errorMessage"] = None

        # Perform the compression itself.
        decompressor = Lz78Decompressor()
        try:
            result["steps"] = decompressor.decompress(compressed_data)
        except ValueError as exception:
            result["errorMessage"] = str(exception)
            return result

        # Save the number of steps needed for decompression.
        result["stepCount"] = len(result["steps"])

        # Return the resulting dictionary.
        return result
