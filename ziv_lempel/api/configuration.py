#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Configuration values for the API.

These values will be used to limit the range of buffer sizes and the length of strings.
"""


class Lz77Configuration:
    """
    The configuration for the LZ77 API.
    """

    SEARCH_BUFFER_SIZE_MIN = 1
    """
    The minimum size of the search buffer.

    :type: :class:`int`
    """

    SEARCH_BUFFER_SIZE_MAX = 15
    """
    The maximum size of the search buffer.

    :type: :class:`int`
    """

    LOOK_AHEAD_BUFFER_SIZE_MIN = 1
    """
    The minimum size of the look-ahead buffer.

    :type: :class:`int`
    """

    LOOK_AHEAD_BUFFER_SIZE_MAX = 15
    """
    The maximum size of the look-ahead buffer.

    :type: :class:`int`
    """

    INPUT_TEXT_LENGTH_MAX = 100
    """
    The maximum length of the input text (uncompressed).

    :type: :class:`int`
    """

    INITIAL_SEARCH_BUFFER_LENGTH_MAX = 20
    """
    The maximum length of the initial search buffer (already compressed).

    :type: :class:`int`
    """

    COMPRESSED_DATA_LENGTH_MAX = 300
    """
    The maximum length of the compressed data (string of triples) when encountered as
    the input during the decompression.

    :type: :class:`int`
    """

    RECENTLY_DECOMPRESSED_LENGTH_MAX = 20
    """
    The maximum length of the recently decompressed text during the decompression.

    :type: :class:`int`
    """


class Lz78Configuration:
    """
    The configuration for the LZ78 API.
    """

    INPUT_TEXT_LENGTH_MAX = 100
    """
    The maximum length of the input text (uncompressed).

    :type: :class:`int`
    """

    COMPRESSED_DATA_LENGTH_MAX = 300
    """
    The maximum length of the compressed data (string of couples) when encountered as
    the input during the decompression.

    :type: :class:`int`
    """


class LzwConfiguration:
    """
    The configuration for the LZW API.
    """

    INPUT_TEXT_LENGTH_MAX = 100
    """
    The maximum length of the input text (uncompressed).

    :type: :class:`int`
    """

    COMPRESSED_DATA_LENGTH_MAX = 100
    """
    The maximum length of the compressed data (string of integers) when encountered as
    the input during the decompression.

    :type: :class:`int`
    """

    INITIAL_DICTIONARY_LENGTH_MAX = 100
    """
    The maximum length of the initial dictionary.

    :type: :class:`int`
    """
