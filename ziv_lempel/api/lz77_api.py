#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Dictionary based API for the LZ77 algorithm.
"""

from ziv_lempel.algorithms.lz77 import Lz77Decompressor, Lz77Compressor
from ziv_lempel.api.configuration import Lz77Configuration as config


class Lz77Api:
    """
    Methods for converting JSON-based API data for the LZ77 algorithm into a format
    which can be processed by Python.

    This class should be used in conjunction with the web-based API when the
    application is served by Flask, where all parameters are wrapped into a dictionary.
    :class:`~ziv_lempel.algorithms.lz77.Lz77Compressor` and
    :class:`~ziv_lempel.algorithms.lz77.Lz77Decompressor` use real method parameters for
    using this implementation inside another Python module - they return a regular
    Python dictionary with each of the steps instead of a dictionary where the steps
    are just a part of and do not incorporate length limitations for the input.
    """

    @staticmethod
    def compress(request_dict):
        """
        Perform LZ77 compression.

        :param request_dict: The data to use as input.
        :type request_dict: dict

        :return: The compression result or an error message.
        :rtype: dict
        """
        # Prepare the result dictionary.
        result = {}

        # Handle empty data.
        if not request_dict:
            result["errorMessage"] = "No parameters passed."
            return result

        # Retrieve the search buffer size.
        try:
            search_buffer_size = int(request_dict.get("searchBufferSize", None))
            if (
                not search_buffer_size
                or search_buffer_size < config.SEARCH_BUFFER_SIZE_MIN
                or search_buffer_size > config.SEARCH_BUFFER_SIZE_MAX
            ):
                result["errorMessage"] = "Search buffer size out of range."
                return result
        except (ValueError, TypeError):
            result["errorMessage"] = "Invalid search buffer size."
            return result

        # Retrieve the look-ahead buffer size.
        try:
            look_ahead_buffer_size = int(request_dict.get("lookAheadBufferSize"))
            if (
                not look_ahead_buffer_size
                or look_ahead_buffer_size < config.LOOK_AHEAD_BUFFER_SIZE_MIN
                or look_ahead_buffer_size > config.LOOK_AHEAD_BUFFER_SIZE_MAX
            ):
                result["errorMessage"] = "Look-ahead buffer size out of range."
                return result
        except (ValueError, TypeError):
            result["errorMessage"] = "Invalid look-ahead buffer size."
            return result

        # Retrieve the input text.
        input_text = request_dict.get("inputText")
        if input_text and len(input_text) > config.INPUT_TEXT_LENGTH_MAX:
            result["errorMessage"] = "Input text too long."
            return result

        # Retrieve the initial search buffer content.
        initial_search_buffer = request_dict.get("initialSearchBuffer")
        if (
            initial_search_buffer
            and len(initial_search_buffer) > config.INITIAL_SEARCH_BUFFER_LENGTH_MAX
        ):
            result["errorMessage"] = "Initial search buffer too large."
            return result

        # There has not been an error.
        result["errorMessage"] = None

        # Respond with the parsed size values.
        result["searchBufferSize"] = search_buffer_size
        result["lookAheadBufferSize"] = look_ahead_buffer_size

        # Perform the compression itself.
        compressor = Lz77Compressor(search_buffer_size, look_ahead_buffer_size)
        result["steps"] = compressor.compress(input_text, initial_search_buffer)

        # Save the number of steps needed for compression.
        result["stepCount"] = len(result["steps"])

        # Return the resulting dictionary.
        return result

    @staticmethod
    def decompress(request_dict):
        """
        Perform LZ77 decompression.

        :param request_dict: The data to use as input.
        :type request_dict: dict

        :return: The decompression result or an error message.
        :rtype: dict
        """
        # Prepare the result dictionary.
        result = {}

        # Handle empty data.
        if not request_dict:
            result["errorMessage"] = "No parameters passed."
            return result

        # Retrieve the search buffer size.
        try:
            search_buffer_size = int(request_dict.get("searchBufferSize", None))
            if (
                not search_buffer_size
                or search_buffer_size < config.SEARCH_BUFFER_SIZE_MIN
                or search_buffer_size > config.SEARCH_BUFFER_SIZE_MAX
            ):
                result["errorMessage"] = "Search buffer size out of range."
                return result
        except (ValueError, TypeError):
            result["errorMessage"] = "Invalid search buffer size."
            return result

        # Retrieve the look-ahead buffer size.
        try:
            look_ahead_buffer_size = int(request_dict.get("lookAheadBufferSize"))
            if (
                not look_ahead_buffer_size
                or look_ahead_buffer_size < config.LOOK_AHEAD_BUFFER_SIZE_MIN
                or look_ahead_buffer_size > config.LOOK_AHEAD_BUFFER_SIZE_MAX
            ):
                result["errorMessage"] = "Look-ahead buffer size out of range."
                return result
        except (ValueError, TypeError):
            result["errorMessage"] = "Invalid look-ahead buffer size."
            return result

        # Retrieve the compressed data triples.
        compressed_data = request_dict.get("compressedData")
        if compressed_data and len(compressed_data) > config.COMPRESSED_DATA_LENGTH_MAX:
            result["errorMessage"] = "Compressed data (triples) too long."
            return result

        # Retrieve the recently decompressed data.
        recently_decompressed = request_dict.get("recentlyDecompressed")
        if (
            recently_decompressed
            and len(recently_decompressed) > config.RECENTLY_DECOMPRESSED_LENGTH_MAX
        ):
            result["errorMessage"] = "Recently decompressed data too long."
            return result

        # There has not been an error.
        result["errorMessage"] = None

        # Respond with the parsed size values.
        result["searchBufferSize"] = search_buffer_size
        result["lookAheadBufferSize"] = look_ahead_buffer_size

        # Perform the compression itself.
        decompressor = Lz77Decompressor(search_buffer_size, look_ahead_buffer_size)
        try:
            result["steps"] = decompressor.decompress(
                compressed_data, recently_decompressed
            )
        except ValueError as exception:
            result["errorMessage"] = str(exception)
            return result

        # Save the number of steps needed for decompression.
        result["stepCount"] = len(result["steps"])

        # Return the resulting dictionary.
        return result
