#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.algorithms.lzw` module.
"""

import unittest

from ziv_lempel.algorithms.lzw import LzwCompressor, LzwDecompressor


class CompressorTests(unittest.TestCase):
    """
    Tests to ensure the correct behaviour of the compression.
    """

    def test_compress_no_initial_dictionary(self):
        """
        Test the output of one compression step without an initial dictionary
        specified.
        """
        compressor = LzwCompressor()
        steps, initial_dictionary = compressor.compress("acrarrarra")
        step = steps[0]
        dictionary_update = step["dictionaryUpdate"]

        self.assertEqual(dictionary_update["index"], 4)
        self.assertEqual(dictionary_update["entry"], "ac")
        self.assertEqual(step["output"], 1)
        self.assertEqual(initial_dictionary, {1: "a", 2: "c", 3: "r"})

    def test_compress_initial_dictionary(self):
        """
        Test the output of one compression step with an initial dictionary specified.
        """
        compressor = LzwCompressor()
        steps, initial_dictionary = compressor.compress("acrarrarra", "car")
        step = steps[0]
        dictionary_update = step["dictionaryUpdate"]

        self.assertEqual(dictionary_update["index"], 4)
        self.assertEqual(dictionary_update["entry"], "ac")
        self.assertEqual(step["output"], 2)
        self.assertEqual(initial_dictionary, {1: "c", 2: "a", 3: "r"})

    def test_prepare_dictionary_none_given(self):
        """
        Test the dictionary preparation with no initial dictionary being given.
        """
        compressor = LzwCompressor()
        dictionary = compressor._prepare_dictionary("rarrad - RARRAD.", None)
        expected = [" ", "-", ".", "A", "D", "R", "a", "d", "r"]
        self.assertEqual(dictionary, expected)

    def test_prepare_dictionary_initial_given(self):
        """
        Test the dictionary preparation with an initial dictionary being given.
        """
        compressor = LzwCompressor()
        dictionary = compressor._prepare_dictionary("rarrad - RARRAD.", "adrADR -.")
        expected = ["a", "d", "r", "A", "D", "R", " ", "-", "."]
        self.assertEqual(dictionary, expected)

    def test_prepare_dictionary_incomplete_given(self):
        """
        Test the dictionary preparation with an incomplete initial dictionary being
        given.
        """
        compressor = LzwCompressor()
        try:
            _ = compressor._prepare_dictionary("rarrad ", "adr")
            self.fail("Missing whitespace inside the dictionary not detected.")
        except ValueError as error:
            self.assertEqual(
                "Text contains ' ' which is not part of the initial dictionary.",
                str(error),
            )

    def test_prepare_dictionary_duplicates_given(self):
        """
        Test the dictionary preparation with an initial dictionary containing
        duplicates being given.
        """
        compressor = LzwCompressor()
        try:
            _ = compressor._prepare_dictionary("rarrad", "adrd")
            self.fail("Duplicate entry for 'r' not detected inside dictionary.")
        except ValueError as error:
            self.assertEqual("Duplicates inside initial dictionary.", str(error))


class DecompressorTests(unittest.TestCase):
    """
    Tests to ensure the correct behaviour of the decompression.
    """

    def test_decompress_example1(self):
        """
        Test the output of one decompression step.
        """
        decompressor = LzwDecompressor()
        steps, initial_dictionary = decompressor.decompress("1", "acr")
        step = steps[0]

        self.assertEqual(step["index"], 1)
        self.assertEqual(step["newCharacters"], "a")
        self.assertEqual(step["strategy"], "normalReference")
        self.assertEqual(step["savedString"], "a")
        self.assertIsNone(step["dictionaryUpdate"])
        self.assertEqual(initial_dictionary, {1: "a", 2: "c", 3: "r"})

    def test_decompress_example2(self):
        """
        Test the output of one decompression step with a different dictionary order.
        """
        decompressor = LzwDecompressor()
        steps, initial_dictionary = decompressor.decompress("2", "car")
        step = steps[0]

        self.assertEqual(step["index"], 2)
        self.assertEqual(step["newCharacters"], "a")
        self.assertEqual(step["strategy"], "normalReference")
        self.assertEqual(step["savedString"], "a")
        self.assertIsNone(step["dictionaryUpdate"])
        self.assertEqual(initial_dictionary, {1: "c", 2: "a", 3: "r"})

    def test_decompress_too_large_index(self):
        """
        Test the decompression with a too large dictionary index.
        """
        decompressor = LzwDecompressor()
        try:
            _, __ = decompressor.decompress("2 10", "acr")
            self.fail("Too large index not detected.")
        except ValueError as error:
            self.assertEqual("'10' references an invalid dictionary entry.", str(error))

    def test_prepare_dictionary_valid_given(self):
        """
        Test the dictionary preparation with a valid initial dictionary.
        """
        decompressor = LzwDecompressor()
        dictionary = decompressor._prepare_dictionary("car")
        expected = ["c", "a", "r"]
        self.assertEqual(dictionary, expected)

    def test_prepare_dictionary_duplicates_given(self):
        """
        Test the dictionary preparation with an initial dictionary containing
        duplicates.
        """
        decompressor = LzwDecompressor()
        try:
            _ = decompressor._prepare_dictionary("cara")
            self.fail("Duplicate entry for 'a' not detected inside dictionary.")
        except ValueError as error:
            self.assertEqual("Duplicates inside initial dictionary.", str(error))

    def test_split_into_indices_one_element_only(self):
        """
        Test the index splitter with one element only.
        """
        decompressor = LzwDecompressor()
        indices = decompressor._split_into_indices("42")
        self.assertEqual(indices, [42])

    def test_split_into_indices_multiple_elements(self):
        """
        Test the index splitter with multiple elements.
        """
        decompressor = LzwDecompressor()
        indices = decompressor._split_into_indices("42 3 25 60 1")
        self.assertEqual(indices, [42, 3, 25, 60, 1])

    def test_split_into_indices_no_integers(self):
        """
        Test the index splitter with values which are no integers.
        """
        decompressor = LzwDecompressor()

        try:
            _ = decompressor._split_into_indices("a")
            self.fail("'a' not detected as invalid index.")
        except ValueError as error:
            self.assertIn("is not a valid index.", str(error))

        try:
            _ = decompressor._split_into_indices("1  3")
            self.fail("' ' not detected as invalid index.")
        except ValueError as error:
            self.assertIn("is not a valid index.", str(error))

    def test_split_into_indices_too_small_values(self):
        """
        Test the index splitter with too small values (less than 1).
        """
        decompressor = LzwDecompressor()

        try:
            _ = decompressor._split_into_indices("0")
            self.fail("'0' not detected as too small index.")
        except ValueError as error:
            self.assertIn(
                "is smaller than 1 and therefore an invalid index.", str(error)
            )

        try:
            _ = decompressor._split_into_indices("-1")
            self.fail("'-1' not detected as too small index.")
        except ValueError as error:
            self.assertIn(
                "is smaller than 1 and therefore an invalid index.", str(error)
            )
