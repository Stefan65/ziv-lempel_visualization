#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.algorithms.utils` module.
"""

import unittest

from ziv_lempel.algorithms import utils


class BestMatchFinderTests(unittest.TestCase):
    """
    Tests for the
    :func:`ziv_lempel.algorithms.utils.find_best_match_in_dictionary_list` method.
    """

    def test_empty_list(self):
        """
        Test with an empty dictionary list.
        """
        index, length = utils.find_best_match_in_dictionary_list([], "abc")
        self.assertEqual(index, -1)
        self.assertEqual(length, 0)

    def test_no_match_list(self):
        """
        Test with no match.
        """
        index, length = utils.find_best_match_in_dictionary_list(["z"], "abc")
        self.assertEqual(index, -1)
        self.assertEqual(length, 0)

    def test_empty_text(self):
        """
        Test with an empty text.
        """
        index, length = utils.find_best_match_in_dictionary_list(["a"], "")
        self.assertEqual(index, -1)
        self.assertEqual(length, 0)

    def test_multiple_matches(self):
        """
        Test with multiple matches.
        """
        dictionary = ["a", "b", "bc", "abc", "de", "ab"]
        index, length = utils.find_best_match_in_dictionary_list(dictionary, "abcd")
        self.assertEqual(index, 3)
        self.assertEqual(length, 3)


class DictionaryConversionTests(unittest.TestCase):
    """
    Tests for the
    :func:`ziv_lempel.algorithms.utils.convert_dictionary_list_to_real_dictionary`
    method.
    """

    def test_empty_list(self):
        """
        Test with an empty dictionary list.
        """
        result = utils.convert_dictionary_list_to_real_dictionary([])
        self.assertEqual(result, dict())

    def test_one_element(self):
        """
        Test with one element only.
        """
        result = utils.convert_dictionary_list_to_real_dictionary(["string"])
        self.assertEqual(result, {1: "string"})

    def test_multiple_elements(self):
        """
        Test with multiple elements.
        """
        dictionary_list = ["a", "b", "bc", "abc", "de", "ab"]
        result = utils.convert_dictionary_list_to_real_dictionary(dictionary_list)
        dictionary = {1: "a", 2: "b", 3: "bc", 4: "abc", 5: "de", 6: "ab"}
        self.assertEqual(result, dictionary)


class CharacterMaskingTests(unittest.TestCase):
    """
    Tests for the :func:`ziv_lempel.algorithms.utils.mask_character` method.
    """

    def test_apostrophe(self):
        """
        Test with an apostrophe.
        """
        result = utils.mask_character("'")
        self.assertEqual(result, "''")

    def test_quotation_mark(self):
        """
        Test with a quotation mark.
        """
        result = utils.mask_character('"')
        self.assertEqual(result, '"')

    def test_backslash(self):
        """
        Test with a backslash.
        """
        result = utils.mask_character("\\")
        self.assertEqual(result, "\\")

    def test_others(self):
        """
        Test with different characters.
        """
        characters = (
            "abcdefghijklmnopqrstuvwxyz1234567890!§$%&/()=?{[]},;.:-_<>|+*~#²³^°"
        )
        characters = characters + characters.upper()
        for character in characters:
            self.assertEqual(utils.mask_character(character), character)
