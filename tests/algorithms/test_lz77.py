#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.algorithms.lz77` module.
"""

import unittest

from ziv_lempel.algorithms.lz77 import Lz77Compressor, Lz77Decompressor


class CompressorTests(unittest.TestCase):
    """
    Tests to ensure the correct behaviour of the compression.
    """

    def test_compress_single_step(self):
        """
        Test the output of the first compression step using an initial search buffer.
        """
        compressor = Lz77Compressor(8, 7)
        result = compressor.compress("acrarrarra", " acra ae")
        result_first_step = result[0]
        result_output = result_first_step["output"]
        self.assertEqual(result_output["offset"], 7)
        self.assertEqual(result_output["length"], 4)
        self.assertEqual(result_output["character"], "r")

    def test_compress_overlap(self):
        """
        Test the output of the first compression step using an initial search buffer
        and having an overlap at the buffer border.
        """
        compressor = Lz77Compressor(7, 6)
        result = compressor.compress("rarrad", "adabrar")
        result_first_step = result[0]
        result_output = result_first_step["output"]
        self.assertEqual(result_output["offset"], 3)
        self.assertEqual(result_output["length"], 5)
        self.assertEqual(result_output["character"], "d")

    def test_get_search_buffer_part_enough_characters(self):
        """
        Test the search buffer part getter with enough characters (more characters
        than the search buffer size) available.
        """
        compressor = Lz77Compressor(5, 1)
        result = compressor._get_search_buffer_part("abcdefghi")
        self.assertEqual(result, "efghi")

    def test_get_search_buffer_part_exact_character_count(self):
        """
        Test the search buffer part getter with an input string having the size of the
        search buffer as specified in the constructor.
        """
        compressor = Lz77Compressor(5, 1)
        result = compressor._get_search_buffer_part("abcde")
        self.assertEqual(result, "abcde")

    def test_get_search_buffer_part_not_enough_characters(self):
        """
        Test the search buffer part getter with a string which is shorter than the size
        of the search buffer.
        """
        compressor = Lz77Compressor(5, 1)
        result = compressor._get_search_buffer_part("abc")
        self.assertEqual(result, "abc")

    def test_get_search_buffer_part_empty(self):
        """
        Test the search buffer part getter with an empty input string.
        """
        compressor = Lz77Compressor(5, 1)
        result = compressor._get_search_buffer_part("")
        self.assertEqual(result, "")

    def test_get_look_ahead_buffer_part_enough_characters(self):
        """
        Test the look-ahead buffer part getter with enough characters (more
        characters than the look-ahead buffer size) available.
        """
        compressor = Lz77Compressor(1, 5)
        result = compressor._get_look_ahead_buffer_part("abcdefghi")
        self.assertEqual(result, "abcde")
        result = compressor._get_look_ahead_buffer_part("abcdefghi", offset=2)
        self.assertEqual(result, "cdefg")

    def test_get_look_ahead_buffer_part_exact_character_count(self):
        """
        Test the look-ahead buffer part getter with an input string having the size of
        the look-ahead buffer as specified in the constructor.
        """
        compressor = Lz77Compressor(1, 5)
        result = compressor._get_look_ahead_buffer_part("abcde")
        self.assertEqual(result, "abcde")
        result = compressor._get_look_ahead_buffer_part("abcde", offset=2)
        self.assertEqual(result, "cde")

    def test_get_look_ahead_buffer_part_not_enough_characters(self):
        """
        Test the look-ahead buffer part getter with a string which is shorter than the
        size of the look-ahead buffer.
        """
        compressor = Lz77Compressor(1, 5)
        result = compressor._get_look_ahead_buffer_part("abc")
        self.assertEqual(result, "abc")
        result = compressor._get_look_ahead_buffer_part("abc", offset=2)
        self.assertEqual(result, "c")

    def test_get_look_ahead_buffer_part_empty(self):
        """
        Test the look-ahead buffer part getter with an empty input string.
        """
        compressor = Lz77Compressor(5, 1)
        result = compressor._get_look_ahead_buffer_part("")
        self.assertEqual(result, "")
        result = compressor._get_look_ahead_buffer_part("", offset=2)
        self.assertEqual(result, "")

    def test_convert_indices_to_offsets(self):
        """
        Test the conversion from indices to offsets.
        """
        compressor = Lz77Compressor(5, 5)
        indices = [0, 1, 2, 3, 4]
        offsets = [5, 4, 3, 2, 1]
        result = compressor._convert_indices_to_offsets("abcde", indices)
        self.assertEqual(result, offsets)

    def test_find_match_lengths_one_match(self):
        """
        Test the match lengths finder with one match.
        """
        compressor = Lz77Compressor(5, 5)
        result = compressor._find_match_lengths("abcde", "abcfg", [0])
        self.assertEqual(result, [3])

    def test_find_match_lengths_multiple_matches(self):
        """
        Test the match length finder with multiple matches.
        """
        compressor = Lz77Compressor(5, 5)
        result = compressor._find_match_lengths("abaac", "abdef", [0, 2, 3])
        self.assertEqual(result, [2, 1, 1])

    def test_find_match_lengths_overlap(self):
        """
        Test the match length finder with a buffer overlap.
        """
        compressor = Lz77Compressor(5, 5)
        result = compressor._find_match_lengths("abcbc", "bcbcb", [1])
        self.assertEqual(result, [5])

    def test_find_best_match_only_match(self):
        """
        Test the best match finder with only one match available.
        """
        compressor = Lz77Compressor(1, 1)
        result_index, result_cause = compressor._find_best_match([1])
        self.assertEqual(result_index, 0)
        self.assertEqual(result_cause, "onlyMatch")

    def test_find_best_match_one_length_only(self):
        """
        Test the best match finder with one length only occurring multiple times.
        """
        compressor = Lz77Compressor(1, 1)
        result_index, result_cause = compressor._find_best_match([2, 2, 2, 2])
        self.assertEqual(result_index, 3)
        self.assertEqual(result_cause, "smallestOffset")

    def test_find_best_match_recurring_lengths(self):
        """
        Test the best match finder with the best match length occurring multiple times.
        """
        compressor = Lz77Compressor(1, 1)
        result_index, result_cause = compressor._find_best_match([2, 1, 2])
        self.assertEqual(result_index, 2)
        self.assertEqual(result_cause, "smallestOffset")

    def test_find_best_match_multiple_lengths(self):
        """
        Test the best match finder with multiple lengths and the best length occurring
        only once.
        """
        compressor = Lz77Compressor(1, 1)
        result_index, result_cause = compressor._find_best_match([2, 1, 3, 2])
        self.assertEqual(result_index, 2)
        self.assertEqual(result_cause, "longestMatch")

    def test_find_all_one_match(self):
        """
        Test the single character match finder method with one match only.
        """
        compressor = Lz77Compressor(1, 1)
        result = compressor._find_all("abcdabcaba", "d")
        self.assertEqual(result, [3])

    def test_find_all_no_match(self):
        """
        Test the single character match finder method with no match at all.
        """
        compressor = Lz77Compressor(1, 1)
        result = compressor._find_all("abcdabcaba", "e")
        self.assertEqual(result, list())

    def test_find_all_multiple_matches(self):
        """
        Test the single character match finder method with multiple matches.
        """
        compressor = Lz77Compressor(1, 1)
        result = compressor._find_all("abcdabcaba", "a")
        self.assertEqual(result, [0, 4, 7, 9])

    def test_find_all_case_mismatch(self):
        """
        Test the single character match finder method with a character which occurs
        inside the text, but with the wrong case.
        """
        compressor = Lz77Compressor(1, 1)
        result = compressor._find_all("abcdabcaba", "D")
        self.assertEqual(result, list())


class DecompressorTests(unittest.TestCase):
    """
    Tests to ensure the correct behaviour of the decompression.
    """

    def test_decompress_single_step(self):
        """
        Test the output of the first decompression step using an initial search buffer.
        """
        decompressor = Lz77Decompressor(8, 7)
        result = decompressor.decompress("(7,4,'r')", " acra ae")
        result = result[0]
        triple = result["triple"]

        self.assertEqual(result["recentlyDecompressed"], " acra ae")
        self.assertIsNone(result["overflow"])
        self.assertEqual(result["strategy"], "simpleCopy")
        self.assertEqual(result["newCharacters"], "acrar")
        self.assertEqual(triple["offset"], 7)
        self.assertEqual(triple["length"], 4)
        self.assertEqual(triple["character"], "r")

    def test_decompress_overlap(self):
        """
        Test the output of the first decompression step using an initial search buffer
        and having an overlap at the buffer border.
        """
        decompressor = Lz77Decompressor(7, 6)
        result = decompressor.decompress("(3,5,'d')", "adabrar")
        result = result[0]
        triple = result["triple"]

        self.assertEqual(result["recentlyDecompressed"], "adabrar")
        self.assertEqual(result["overflow"], "ra")
        self.assertEqual(result["strategy"], "continuePattern")
        self.assertEqual(result["newCharacters"], "rarrad")
        self.assertEqual(triple["offset"], 3)
        self.assertEqual(triple["length"], 5)
        self.assertEqual(triple["character"], "d")

    def test_decompress_large_overflow(self):
        """
        Test the output of the first decompression step using an initial search buffer
        and having a large overlap at the buffer border.
        """
        decompressor = Lz77Decompressor(10, 10)
        result = decompressor.decompress("(1,9,'a')", "a")
        result = result[0]
        triple = result["triple"]

        self.assertEqual(result["recentlyDecompressed"], "a")
        self.assertEqual(result["overflow"], "a" * 8)
        self.assertEqual(result["strategy"], "continuePattern")
        self.assertEqual(result["newCharacters"], "a" * 10)
        self.assertEqual(triple["offset"], 1)
        self.assertEqual(triple["length"], 9)
        self.assertEqual(triple["character"], "a")

    def test_get_recently_decompressed_part_enough_characters(self):
        """
        Test the recently decompressed part getter with enough characters (more
        characters than the search buffer size) available.
        """
        decompressor = Lz77Decompressor(5, 1)
        result = decompressor._get_recently_decompressed_part("abcdefghi")
        self.assertEqual(result, "efghi")

    def test_get_recently_decompressed_part_exact_character_count(self):
        """
        Test the recently decompressed part getter with an input string having the size
        of the search buffer as specified in the constructor.
        """
        decompressor = Lz77Decompressor(5, 1)
        result = decompressor._get_recently_decompressed_part("abcde")
        self.assertEqual(result, "abcde")

    def test_get_recently_decompressed_part_not_enough_characters(self):
        """
        Test the recently decompressed part getter with a string which is shorter than
        the size of the search buffer.
        """
        decompressor = Lz77Decompressor(5, 1)
        result = decompressor._get_recently_decompressed_part("abc")
        self.assertEqual(result, "abc")

    def test_split_into_triples_empty_input(self):
        """
        Test the triple splitter with an empty input string.
        """
        decompressor = Lz77Decompressor(1, 1)
        result = decompressor._split_into_triples("")
        self.assertEqual(result, [""])

    def test_split_into_triples_one_triple_only(self):
        """
        Test the triple splitter with one triple only.
        """
        decompressor = Lz77Decompressor(1, 1)
        result = decompressor._split_into_triples("(a, b, c)")
        self.assertEqual(result, ["(a, b, c)"])

    def test_split_into_triples_one_triple_only_padded(self):
        """
        Test the triple splitter with one padded triple.
        """
        decompressor = Lz77Decompressor(1, 1)
        result = decompressor._split_into_triples("  (a, b, c)  ")
        self.assertEqual(result, ["(a, b, c)"])

    def test_split_into_triples_multiple_triples(self):
        """
        Test the triple splitter with multiple triples.
        """
        decompressor = Lz77Decompressor(1, 1)
        text = "  (a, b, c) (b, c, d)(d, e, f)(e, f, g) (h, i, j)  "
        result = decompressor._split_into_triples(text)
        triples = ["(a, b, c", "b, c, d", "d, e, f", "e, f, g", "h, i, j)"]
        self.assertEqual(result, triples)

    def test_extract_triple_elements_spaced(self):
        """
        Test the triple elements extractor with the elements separated by spaces.
        """
        decompressor = Lz77Decompressor(5, 6)

        offset, length, character = decompressor._extract_triple_elements("(1, 2, 'a')")
        self.assertEqual(offset, 1)
        self.assertEqual(length, 2)
        self.assertEqual(character, "a")

        offset, length, character = decompressor._extract_triple_elements(
            "(1, 2, C('a'))"
        )
        self.assertEqual(offset, 1)
        self.assertEqual(length, 2)
        self.assertEqual(character, "a")

    def test_extract_triple_elements_not_spaced(self):
        """
        Test the triple elements extractor with the elements separated by commas only.
        """
        decompressor = Lz77Decompressor(5, 6)

        offset, length, character = decompressor._extract_triple_elements("(1,2,'a')")
        self.assertEqual(offset, 1)
        self.assertEqual(length, 2)
        self.assertEqual(character, "a")

        offset, length, character = decompressor._extract_triple_elements(
            "(1,2,C('a'))"
        )
        self.assertEqual(offset, 1)
        self.assertEqual(length, 2)
        self.assertEqual(character, "a")

    def test_extract_triple_elements_character_is_comma(self):
        """
        Test the triple elements extractor with the character being a comma.
        """
        decompressor = Lz77Decompressor(5, 6)

        offset, length, character = decompressor._extract_triple_elements("(1, 2, ',')")
        self.assertEqual(offset, 1)
        self.assertEqual(length, 2)
        self.assertEqual(character, ",")

        offset, length, character = decompressor._extract_triple_elements(
            "(1, 2, C(','))"
        )
        self.assertEqual(offset, 1)
        self.assertEqual(length, 2)
        self.assertEqual(character, ",")

    def test_extract_triple_elements_no_triple(self):
        """
        Test the triple elements extractor with a tuple.
        """
        decompressor = Lz77Decompressor(1, 1)
        try:
            _ = decompressor._extract_triple_elements("(1, 2)")
            self.fail("Detected tuple as triple.")
        except ValueError as error:
            self.assertIn("is not a triple.", str(error))

    def test_extract_triple_elements_invalid_offset(self):
        """
        Test the triple elements extractor with invalid offset values.
        """
        decompressor = Lz77Decompressor(1, 5)

        try:
            _ = decompressor._extract_triple_elements("('a', 2, 1)")
            self.fail("Non-number offset not detected.")
        except ValueError as error:
            self.assertIn("has an invalid offset value.", str(error))

        try:
            _ = decompressor._extract_triple_elements("(-1, 2, 'a')")
            self.fail("Negative offset not detected.")
        except ValueError as error:
            self.assertIn("has an offset value out of bounds.", str(error))

        try:
            _ = decompressor._extract_triple_elements("(2, 2, 'a')")
            self.fail("Too large offset not detected.")
        except ValueError as error:
            self.assertIn(
                "has an offset which does not match the search buffer size.", str(error)
            )

    def test_extract_triple_elements_invalid_length(self):
        """
        Test the triple elements extractor with invalid length values.
        """
        decompressor = Lz77Decompressor(5, 1)

        try:
            _ = decompressor._extract_triple_elements("(2, 'a', 1)")
            self.fail("Non-number length not detected.")
        except ValueError as error:
            self.assertIn("has an invalid length value.", str(error))

        try:
            _ = decompressor._extract_triple_elements("(2, -1, 'a')")
            self.fail("Negative length not detected.")
        except ValueError as error:
            self.assertIn("has a length value out of bounds.", str(error))

        try:
            _ = decompressor._extract_triple_elements("(2, 2, 'a')")
            self.fail("Too large length not detected.")
        except ValueError as error:
            self.assertIn("has a length value out of bounds.", str(error))

    def test_extract_triple_elements_invalid_offset_and_length_combination(self):
        """
        Test the triple elements extractor with invalid length and offset value
        combinations.
        """
        decompressor = Lz77Decompressor(5, 5)

        try:
            _ = decompressor._extract_triple_elements("(0, 1, 'a')")
            self.fail("Length cannot be specified for an offset of zero.")
        except ValueError as error:
            self.assertIn(
                "has an invalid combination of length and offset values.", str(error)
            )

        try:
            _ = decompressor._extract_triple_elements("(1, 0, 'a')")
            self.fail("Offset cannot be specified for a length of zero.")
        except ValueError as error:
            self.assertIn(
                "has an invalid combination of length and offset values.", str(error)
            )

    def test_extract_triple_elements_unmasking(self):
        """
        Test the triple elements extractor with a masked character.
        """
        decompressor = Lz77Decompressor(1, 1)

        _, __, character = decompressor._extract_triple_elements("(0, 0, '''')")
        self.assertEqual(character, "'")

        _, __, character = decompressor._extract_triple_elements("(0, 0, C(''''))")
        self.assertEqual(character, "'")
