#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.algorithms.lz78` module.
"""

import unittest

from ziv_lempel.algorithms.lz78 import Lz78Compressor, Lz78Decompressor


class CompressorTests(unittest.TestCase):
    """
    Tests to ensure the correct behaviour of the compression.
    """

    def test_compress_example1(self):
        """
        Test the output of one compression step.
        """
        compressor = Lz78Compressor()
        result = compressor.compress("acrarrarra")
        result = result[4]
        dictionary_update = result["dictionaryUpdate"]
        output = result["output"]

        self.assertEqual(dictionary_update["index"], 5)
        self.assertEqual(dictionary_update["entry"], "ra")
        self.assertEqual(output["index"], 3)
        self.assertEqual(output["character"], "a")
        self.assertEqual(result["strategy"], "bestMatch")

    def test_compress_example2(self):
        """
        Test the output of one compression step.
        """
        compressor = Lz78Compressor()
        result = compressor.compress("rarrad")
        result = result[2]
        dictionary_update = result["dictionaryUpdate"]
        output = result["output"]

        self.assertEqual(dictionary_update["index"], 3)
        self.assertEqual(dictionary_update["entry"], "rr")
        self.assertEqual(output["index"], 1)
        self.assertEqual(output["character"], "r")
        self.assertEqual(result["strategy"], "bestMatch")


class DecompressorTests(unittest.TestCase):
    """
    Tests to ensure the correct behaviour of the decompression.
    """

    def test_decompress_example1(self):
        """
        Test the output of one decompression step.
        """
        decompressor = Lz78Decompressor()
        result = decompressor.decompress(
            "(0, 'a') (0, 'c') (0, 'r') (1, 'r') (3, 'a') (3, 'r') (0, 'a')"
        )
        result = result[4]
        dictionary_update = result["dictionaryUpdate"]

        self.assertEqual(dictionary_update["index"], 5)
        self.assertEqual(dictionary_update["entry"], "ra")
        self.assertEqual(result["newCharacters"], "ra")
        self.assertEqual(result["couple"]["index"], 3)
        self.assertEqual(result["couple"]["character"], "a")

    def test_decompress_example2(self):
        """
        Test the output of one decompression step.
        """
        decompressor = Lz78Decompressor()
        result = decompressor.decompress("(0, 'r') (0, 'a') (1, 'r') (2, 'd')")
        result = result[2]
        dictionary_update = result["dictionaryUpdate"]

        self.assertEqual(dictionary_update["index"], 3)
        self.assertEqual(dictionary_update["entry"], "rr")
        self.assertEqual(result["newCharacters"], "rr")
        self.assertEqual(result["couple"]["index"], 1)
        self.assertEqual(result["couple"]["character"], "r")

    def test_decompress_too_large_index(self):
        """
        Test the decompression with a too large dictionary index.
        """
        decompressor = Lz78Decompressor()
        try:
            steps, initial_dictionary = decompressor.decompress("(10, 'a')")
            self.fail("Too large index not detected.")
        except ValueError as error:
            self.assertEqual(
                "\"(10, 'a')\" references an invalid dictionary entry.", str(error)
            )

    def test_split_into_couples_empty_input(self):
        """
        Test the couple splitter with an empty input string.
        """
        decompressor = Lz78Decompressor()
        result = decompressor._split_into_couples("")
        self.assertEqual(result, [""])

    def test_split_into_couples_one_triple_only(self):
        """
        Test the couple splitter with one couple only.
        """
        decompressor = Lz78Decompressor()
        result = decompressor._split_into_couples("(a, b)")
        self.assertEqual(result, ["(a, b)"])

    def test_split_into_couples_one_couple_only_padded(self):
        """
        Test the couple splitter with one padded couple.
        """
        decompressor = Lz78Decompressor()
        result = decompressor._split_into_couples("  (a, b)  ")
        self.assertEqual(result, ["(a, b)"])

    def test_split_into_couples_multiple_couples(self):
        """
        Test the couple splitter with multiple couples.
        """
        decompressor = Lz78Decompressor()
        text = "  (a, b) (b, c)(d, e)(e, f) (h, i)  "
        result = decompressor._split_into_couples(text)
        couples = ["(a, b", "b, c", "d, e", "e, f", "h, i)"]
        self.assertEqual(result, couples)

    def test_extract_couple_elements_spaced(self):
        """
        Test the couple elements extractor with the elements separated by spaces.
        """
        decompressor = Lz78Decompressor()

        index, character = decompressor._extract_couple_elements("(1, 'a')")
        self.assertEqual(index, 1)
        self.assertEqual(character, "a")

        index, character = decompressor._extract_couple_elements("(1, C('a'))")
        self.assertEqual(index, 1)
        self.assertEqual(character, "a")

    def test_extract_couple_elements_not_spaced(self):
        """
        Test the couple elements extractor with the elements separated by commas only.
        """
        decompressor = Lz78Decompressor()

        index, character = decompressor._extract_couple_elements("(1,'a')")
        self.assertEqual(index, 1)
        self.assertEqual(character, "a")

        index, character = decompressor._extract_couple_elements("(1,C('a'))")
        self.assertEqual(index, 1)
        self.assertEqual(character, "a")

    def test_extract_couple_elements_character_is_comma(self):
        """
        Test the couple elements extractor with the character being a comma.
        """
        decompressor = Lz78Decompressor()

        index, character = decompressor._extract_couple_elements("(1, ',')")
        self.assertEqual(index, 1)
        self.assertEqual(character, ",")

        index, character = decompressor._extract_couple_elements("(1, C(','))")
        self.assertEqual(index, 1)
        self.assertEqual(character, ",")

    def test_extract_couple_elements_no_couple(self):
        """
        Test the couple elements extractor with a triple and single value.
        """
        decompressor = Lz78Decompressor()

        try:
            _ = decompressor._extract_couple_elements("(1, 2, 'a')")
            self.fail("Detected triple as couple.")
        except ValueError as error:
            self.assertIn("is not a couple.", str(error))

        try:
            _ = decompressor._extract_couple_elements("(1)")
            self.fail("Detected single value as couple.")
        except ValueError as error:
            self.assertIn("is not a couple.", str(error))

    def test_extract_couple_elements_invalid_index(self):
        """
        Test the couple elements extractor with invalid index values.
        """
        decompressor = Lz78Decompressor()

        try:
            _ = decompressor._extract_couple_elements("('a', 1)")
            self.fail("Non-number index not detected.")
        except ValueError as error:
            self.assertIn("has an invalid index value.", str(error))

        try:
            _ = decompressor._extract_couple_elements("(-1, 'a')")
            self.fail("Negative index not detected.")
        except ValueError as error:
            self.assertIn("has an index value out of bounds.", str(error))

    def test_extract_couple_elements_unmasking(self):
        """
        Test the couple elements extractor with a masked character.
        """
        decompressor = Lz78Decompressor()

        _, character = decompressor._extract_couple_elements("(0, '''')")
        self.assertEqual(character, "'")

        _, character = decompressor._extract_couple_elements("(0, C(''''))")
        self.assertEqual(character, "'")
