#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.api.lz78` module.
"""

from ziv_lempel.api.lz78_api import Lz78Api
from tests.api.utils import JsonTestCase


class JsonApiTests(JsonTestCase):
    """
    Tests for the JSON API class.
    """

    def setUp(self):
        """
        Set the base directory.
        """
        self.base_directory = "files/lz78"

    def test_compression_example1(self):
        """
        Test the compression with a simple example.
        """
        data = {
            "inputText": "ASCII ANSI",
        }
        result = Lz78Api.compress(data)
        self.perform_json_check(result, "compression_example1.json")

    def test_compression_example2(self):
        """
        Test the compression with a simple example.
        """
        data = {
            "inputText": "Just a test.",
        }
        result = Lz78Api.compress(data)
        self.perform_json_check(result, "compression_example2.json")

    def test_decompression_example1(self):
        """
        Test the decompression with a simple example.
        """
        compressed_data = (
            "(0, 'A') (0, 'S') (0, 'C') (0, 'I') (4, ' ') (1, 'N') (2, 'I')"
        )
        data = {
            "compressedData": compressed_data,
        }
        result = Lz78Api.decompress(data)
        self.perform_json_check(result, "decompression_example1.json")

    def test_decompression_example2(self):
        """
        Test the decompression with a simple example.
        """
        compressed_data = (
            "(0, 'J') (0, 'u') (0, 's') (0, 't') (0, ' ') (0, 'a') (5, 't') (0, 'e')"
            + "(3, 't') (0, '.')"
        )
        data = {
            "compressedData": compressed_data,
        }
        result = Lz78Api.decompress(data)
        self.perform_json_check(result, "decompression_example2.json")
