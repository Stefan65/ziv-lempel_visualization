#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
A collection of test utilities.
"""

import json
import os
import unittest


class JsonTestCase(unittest.TestCase):
    """
    Handle JSON-based test cases for complex objects.
    """

    base_directory = ""
    """
    The base directory where the files with the expected output are located.

    :type: :class:`str`
    """

    def load_input_file(self, filename):
        """
        Load the given input file.

        :param filename: The name of the file to get.
        :type filename: str

        :return: The string as read from the given file (appended to the base
                 directory). It gets stripped to avoid final newlines.
        :rtype: str
        """
        # We have to prepend the API tests directory to the path as we are running the
        # tests from the root directory of the Git repository.
        path = os.path.join("tests/api", self.base_directory, filename)
        with open(path, encoding="utf8") as infile:
            # Avoid final newlines by stripping. This is needed as some editors may add
            # final newlines to each file.
            data = infile.read().strip()
        return data

    def perform_json_check(self, data_output, filename_expected):
        """
        Perform a JSON-based check.

        This will produce a JSON representation of the given data output and then check
        for equality with the content of the given file.

        :param data_output: The output data to check for correctness.
        :type data_output: object

        :param filename_expected: The name of the file with the expected JSON
                                  serialization.
        :type filename_expected: str
        """
        # Use the indentation parameter here to allow the expected serialization to be
        # readable without hassle.
        data_json = json.dumps(data_output, indent=4)
        expected_json = self.load_input_file(filename_expected)
        self.assertEqual(data_json, expected_json)
