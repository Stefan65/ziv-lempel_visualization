#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.api.lz77` module.
"""

from ziv_lempel.api.lz77_api import Lz77Api
from tests.api.utils import JsonTestCase


class JsonApiTests(JsonTestCase):
    """
    Tests for the JSON API class.
    """

    def setUp(self):
        """
        Set the base directory.
        """
        self.base_directory = "files/lz77"

    def test_compression_example1(self):
        """
        Test the compression with a simple example.
        """
        data = {
            "searchBufferSize": 7,
            "lookAheadBufferSize": 6,
            "inputText": "ASCII ANSI",
        }
        result = Lz77Api.compress(data)
        self.perform_json_check(result, "compression_example1.json")

    def test_compression_example2(self):
        """
        Test the compression with a simple example.

        Special case in here: The input cannot be compressed and will be bigger
        afterwards.
        """
        data = {
            "searchBufferSize": 7,
            "lookAheadBufferSize": 6,
            "inputText": "abcdefghi" * 2,
        }
        result = Lz77Api.compress(data)
        self.perform_json_check(result, "compression_example2.json")

    def test_compression_example3(self):
        """
        Test the compression with a simple example.

        Special case in here: An initial search buffer is given.
        """
        data = {
            "searchBufferSize": 7,
            "lookAheadBufferSize": 6,
            "inputText": "Lorem ipsum dolor sit amet.",
            "initialSearchBuffer": "NULL;",
        }
        result = Lz77Api.compress(data)
        self.perform_json_check(result, "compression_example3.json")

    def test_decompression_example1(self):
        """
        Test the decompression with a simple example.

        Special case in here: The last match has been reduced by one character as the
        third triple element would be missing/empty otherwise.
        """
        compressed_data = (
            "(0, 0, 'A') (0, 0, 'S') (0, 0, 'C') (0, 0, 'I') (1, 1, ' ') (6, 1, 'N')"
            + "(7, 1, 'I')"
        )
        data = {
            "searchBufferSize": 7,
            "lookAheadBufferSize": 6,
            "compressedData": compressed_data,
        }
        result = Lz77Api.decompress(data)
        self.perform_json_check(result, "decompression_example1.json")

    def test_decompression_example2(self):
        """
        Test the decompression with a simple example.

        Special case in here: There are no matches at all.
        """
        compressed_data = (
            "(0, 0, 'a') (0, 0, 'b') (0, 0, 'c') (0, 0, 'd') (0, 0, 'e') (0, 0, 'f') "
            + "(0, 0, 'g') (0, 0, 'h') (0, 0, 'i')"
        )
        data = {
            "searchBufferSize": 7,
            "lookAheadBufferSize": 6,
            "compressedData": compressed_data * 2,
        }
        result = Lz77Api.decompress(data)
        self.perform_json_check(result, "decompression_example2.json")

    def test_decompression_example3(self):
        """
        Test the decompression with a simple example.

        Special case in here: Some initial data for the search buffer is given.
        """
        compressed_data = (
            "(2, 1, 'o') (0, 0, 'r') (0, 0, 'e') (0, 0, 'm') (0, 0, ' ') (0, 0, 'i') "
            + "(0, 0, 'p') (0, 0, 's') (0, 0, 'u') (6, 2, 'd') (0, 0, 'o') "
            + "(0, 0, 'l') (2, 1, 'r') (6, 1, 's') (0, 0, 'i') (0, 0, 't') "
            + "(4, 1, 'a') (0, 0, 'm') (0, 0, 'e') (5, 1, '.')"
        )
        data = {
            "searchBufferSize": 7,
            "lookAheadBufferSize": 6,
            "compressedData": compressed_data,
            "recentlyDecompressed": "NULL;",
        }
        result = Lz77Api.decompress(data)
        self.perform_json_check(result, "decompression_example3.json")
