#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for the :mod:`ziv_lempel.api.lzw` module.
"""

from ziv_lempel.api.lzw_api import LzwApi
from tests.api.utils import JsonTestCase


class JsonApiTests(JsonTestCase):
    """
    Tests for the JSON API class.
    """

    def setUp(self):
        """
        Set the base directory.
        """
        self.base_directory = "files/lzw"

    def test_compression_example1(self):
        """
        Test the compression with a simple example.
        """
        data = {
            "inputText": "RATATATAT A RAT AT A RAT",
        }
        result = LzwApi.compress(data)
        self.perform_json_check(result, "compression_example1.json")

    def test_compression_example2(self):
        """
        Test the compression with a simple example.
        """
        data = {
            "inputText": "THIS IS HIS HIT",
        }
        result = LzwApi.compress(data)
        self.perform_json_check(result, "compression_example2.json")

    def test_compression_example3(self):
        """
        Test the compression with a simple example.

        Special case in here: The initial dictionary is given, the text is the same as
        for example 2.
        """
        data = {
            "inputText": "THIS IS HIS HIT",
            "initialDictionary": "S ITH",
        }
        result = LzwApi.compress(data)
        self.perform_json_check(result, "compression_example3.json")

    def test_compression_example4(self):
        """
        Test the compression with a simple example.

        Special case in here: During decompression there will be a reference to an
        entry which has not been written yet.
        """
        data = {
            "inputText": "abababa",
        }
        result = LzwApi.compress(data)
        self.perform_json_check(result, "compression_example4.json")

    def test_decompression_example1(self):
        """
        Test the decompression with a simple example.
        """
        data = {
            "compressedData": "3 1 4 6 8 4 2 1 2 5 10 6 11 13 6",
            "initialDictionary": " ART",
        }
        result = LzwApi.decompress(data)
        self.perform_json_check(result, "decompression_example1.json")

    def test_decompression_example2(self):
        """
        Test the decompression with a simple example.
        """
        data = {
            "compressedData": "5 2 3 4 1 8 1 7 9 7 5",
            "initialDictionary": " HIST",
        }
        result = LzwApi.decompress(data)
        self.perform_json_check(result, "decompression_example2.json")

    def test_decompression_example3(self):
        """
        Test the compression with a simple example.

        Special case in here: A different initial dictionary is given, the original
        text is the same as for example 2.
        """
        data = {
            "compressedData": "4 5 3 1 2 8 2 7 9 7 4",
            "initialDictionary": "S ITH",
        }
        result = LzwApi.decompress(data)
        self.perform_json_check(result, "decompression_example3.json")

    def test_decompression_example4(self):
        """
        Test the decompression with a simple example.

        Special case in here: There will be a reference to an entry which has not been
        written yet.
        """
        data = {
            "compressedData": "1 2 3 5",
            "initialDictionary": "ab",
        }
        result = LzwApi.decompress(data)
        self.perform_json_check(result, "decompression_example4.json")
